package fr.syncrase.corporateEMails;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ERefactoredSolution implements EMailGenerator {
    @Override
    public String buildAddressBook(String names, String company) {

        Map<String, Integer> occurenceCounter = new HashMap<>();

        return Stream.of(names.split(", ")).map(name -> {

            String[] nameSplitted, addressParts, domainNameParts, usernameParts;
            String emailAddress, formattedLastName;
            // Build username
            nameSplitted = name.split(" ");
            addressParts = new String[nameSplitted.length + 1];
            System.arraycopy(nameSplitted, 0, addressParts, 0, nameSplitted.length);
            usernameParts = new String[nameSplitted.length];
            for (int j = 0; j < nameSplitted.length - 1; usernameParts[j] = nameSplitted[j].substring(0, 1), j++) {
            }
            formattedLastName = nameSplitted[nameSplitted.length - 1].replace("-", "");
            formattedLastName = formattedLastName.length() > 8 ? formattedLastName.substring(0, 8) : formattedLastName;
            usernameParts[nameSplitted.length - 1] = formattedLastName;

            // Refactoring 5 : Handle duplicates
            String username = Stream.of(usernameParts).collect(Collectors.joining("_"));
            occurenceCounter.compute(username, (k, v) -> (v == null) ? Integer.valueOf(1) : v + 1);
            username = Stream//
                    .of(new String[]{username,
                            occurenceCounter.get(username) > 1 ? occurenceCounter.get(username) + "" : ""})
                    .collect(Collectors.joining());

            // Build domain name
            domainNameParts = new String[]{company, "com"};

            // Build email
            emailAddress = Stream.of(new String[]{ //
                    username, //
                    Stream.of(domainNameParts).collect(Collectors.joining("."))//
            }).collect(Collectors.joining("@", "<", ">")).toLowerCase();

            // Build address
            addressParts[nameSplitted.length] = emailAddress;
            return Stream.of(addressParts).collect(Collectors.joining(" "));
        }).collect(Collectors.joining(", "));

    }
}

package fr.syncrase.corporateEMails;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FinalSolution implements EMailGenerator {

    Map<String, Integer> occurenceCounter = new HashMap<>();

    @Override
    public String buildAddressBook(String names, String company) {

        return Stream.of(names.split(", "))
                .map(name -> buildAddress(company, name.split(" ")))
                .collect(Collectors.joining(", "));
    }

    private String buildAddress(String company, String[] nameSplitted) {
        String[] addressParts = new String[nameSplitted.length + 1];

        // Copy 1st, middle and last name to the address
        System.arraycopy(nameSplitted, 0, addressParts, 0, nameSplitted.length);

        // Add the email
        addressParts[nameSplitted.length] = buildEmail(//
                buildUsername(nameSplitted), //
                buildDomainName(new String[]{company, "com"}) //
        );

        return Stream.of(addressParts).collect(Collectors.joining(" "));
    }

    private String buildDomainName(String[] domainNameParts) {
        return Stream.of(domainNameParts).collect(Collectors.joining("."));
    }

    private String buildEmail(String username, String domainName) {
        return Stream.of(new String[]{ //
                username, //
                domainName//
        }).collect(Collectors.joining("@", "<", ">")).toLowerCase();
    }

    private String buildUsername(String[] nameSplitted) {
        String[] usernameParts = new String[nameSplitted.length];
        for (int j = 0; j < nameSplitted.length - 1; j++) {
            usernameParts[j] = nameSplitted[j].substring(0, 1);
        }
        String formattedLastName = nameSplitted[nameSplitted.length - 1].replace("-", "");
        formattedLastName = formattedLastName.length() > 8 ? formattedLastName.substring(0, 8) : formattedLastName;
        usernameParts[nameSplitted.length - 1] = formattedLastName;
        String username = Stream.of(usernameParts).collect(Collectors.joining("_"));

        occurenceCounter.compute(username, (k, v) -> (v == null) ? Integer.valueOf(1) : v + 1);

        return Stream//
                .of(new String[]{username,
                        occurenceCounter.get(username) > 1 ? occurenceCounter.get(username) + "" : ""})
                .collect(Collectors.joining());

    }
}

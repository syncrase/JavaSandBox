package fr.syncrase.corporateEMails;

public class ARefactoredSolution implements EMailGenerator {

    @Override
    public String buildAddressBook(String names, String company) {
        String[] allNames = names.split(", ");
        StringBuilder sb = new StringBuilder();
        String[] nameSplitted;

        for (int i = 0; i < allNames.length; i++) {
            sb.append(allNames[i]);
            sb.append(" ");
            sb.append("<");

            nameSplitted = allNames[i].split(" ");

            // Refactoring 1 : remove the if/else for handle the lastname => for loop which
            // skip the latest
            for (int j = 0; j < nameSplitted.length - 1; j++) {
                sb.append(nameSplitted[j].toLowerCase().charAt(0));
                sb.append("_");
            }

            String name;
            name = nameSplitted[nameSplitted.length - 1].replace("-", "");
            name = name.length() > 8 ? name.substring(0, 8) : name;
            sb.append(name.toLowerCase());

            sb.append("@");
            sb.append(company.toLowerCase());
            sb.append(".com");
            sb.append(i == allNames.length - 1 ? ">" : ">, ");

        }

        return sb.toString();

    }
}

package fr.syncrase.corporateEMails;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HandleDuplicates implements EMailGenerator {
    @Override
    public String buildAddressBook(String names, String company) {
        String[] allNames = names.split(", ");

        Stream<String> myStream;
        myStream = Arrays.asList(allNames).stream();
        myStream = Stream.of(allNames);
        Map<String, Integer> occurenceCounter = new HashMap<>();

        return myStream//
                .map(name -> {
                    String[] splittedName = name.split(" ");
                    char[] onlyFirstLetters = Stream.of(splittedName).map(nameMonade -> nameMonade.substring(0, 1))
                            .collect(Collectors.joining("_")).toCharArray();

                    String nonTruncatedLastName = splittedName[splittedName.length - 1].replace("-", "");
                    char[] onlyLastName = (nonTruncatedLastName.length() > 8 ? nonTruncatedLastName.substring(0, 8)
                            : nonTruncatedLastName).toCharArray();

                    char[] completeEmailIdArray = new char[onlyFirstLetters.length - 1 + onlyLastName.length];

                    for (int i = 0; i < onlyFirstLetters.length - 1; i++) {
                        completeEmailIdArray[i] = onlyFirstLetters[i];
                    }

                    for (int i = onlyFirstLetters.length - 1; i < completeEmailIdArray.length; i++) {
                        completeEmailIdArray[i] = onlyLastName[i - onlyFirstLetters.length + 1];
                    }

                    String completeEmailId = Stream.of(completeEmailIdArray).map(String::valueOf)
                            .collect(Collectors.joining());
                    occurenceCounter.compute(completeEmailId, (k, v) -> (v == null) ? Integer.valueOf(1) : v + 1);

                    String[] arrForRes = new String[splittedName.length + 1];

                    for (int i = 0; i < splittedName.length; i++) {
                        arrForRes[i] = splittedName[i];
                    }

                    completeEmailId = Stream//
                            .of(new String[]{completeEmailId,
                                    occurenceCounter.get(completeEmailId) > 1
                                            ? occurenceCounter.get(completeEmailId) + ""
                                            : ""})
                            .collect(Collectors.joining());

                    arrForRes[splittedName.length] = Stream//
                            .of(new String[]{completeEmailId, company})//
                            .collect(Collectors.joining("@", "<", ">"))//
                            .toLowerCase();

                    return Stream.of(arrForRes).map(String::valueOf).collect(Collectors.joining(" "));

                })//
                .collect(Collectors.joining(", "));

    }
}

package fr.syncrase.corporateEMails;

public interface EMailGenerator {

    public String buildAddressBook(String names, String company);
}

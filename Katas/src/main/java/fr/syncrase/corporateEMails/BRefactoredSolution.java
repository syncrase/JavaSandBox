package fr.syncrase.corporateEMails;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BRefactoredSolution implements EMailGenerator {
    @Override
    public String buildAddressBook(String names, String company) {
        String[] allNames = names.split(", ");
        StringBuilder sb = new StringBuilder();
        String[] nameSplitted;

        // Refactoring 2 : remove the condition for address separator at the end => use
        // collectors joining
        String[] addressBook = new String[allNames.length];

        for (int i = 0; i < allNames.length; i++) {
            sb.append(allNames[i]);
            sb.append(" ");

            sb.append("<");

            nameSplitted = allNames[i].split(" ");

            for (int j = 0; j < nameSplitted.length - 1; j++) {
                sb.append(nameSplitted[j].toLowerCase().charAt(0));
                sb.append("_");
            }

            String name;
            name = nameSplitted[nameSplitted.length - 1].replace("-", "");
            name = name.length() > 8 ? name.substring(0, 8) : name;
            sb.append(name.toLowerCase());

            sb.append("@");
            sb.append(company.toLowerCase());
            sb.append(".com");
            sb.append(">");
            addressBook[i] = sb.toString();
            sb.setLength(0);
        }

        return Stream.of(addressBook).collect(Collectors.joining(", "));

    }
}

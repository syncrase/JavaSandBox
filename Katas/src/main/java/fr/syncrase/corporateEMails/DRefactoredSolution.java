package fr.syncrase.corporateEMails;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DRefactoredSolution implements EMailGenerator {
    @Override
    public String buildAddressBook(String names, String company) {

        // Refactoring 4 : Remove the outter for loop
        return Stream.of(names.split(", ")).map(name -> {

            String[] nameSplitted, addressParts, domainNameParts, usernameParts;
            String emailAddress, formattedLastName;
            // Build username
            nameSplitted = name.split(" ");
            addressParts = new String[nameSplitted.length + 1];
            System.arraycopy(nameSplitted, 0, addressParts, 0, nameSplitted.length);
            usernameParts = new String[nameSplitted.length];
            for (int j = 0; j < nameSplitted.length - 1; usernameParts[j] = nameSplitted[j].substring(0, 1), j++) {
            }
            formattedLastName = nameSplitted[nameSplitted.length - 1].replace("-", "");
            formattedLastName = formattedLastName.length() > 8 ? formattedLastName.substring(0, 8) : formattedLastName;
            usernameParts[nameSplitted.length - 1] = formattedLastName;

            // Build domain name
            domainNameParts = new String[]{company, "com"};

            // Build email
            emailAddress = Stream.of(new String[]{ //
                    Stream.of(usernameParts).collect(Collectors.joining("_")), //
                    Stream.of(domainNameParts).collect(Collectors.joining("."))//
            }).collect(Collectors.joining("@", "<", ">")).toLowerCase();

            // Build address
            addressParts[nameSplitted.length] = emailAddress;
            return Stream.of(addressParts).collect(Collectors.joining(" "));
        }).collect(Collectors.joining(", "));

    }
}

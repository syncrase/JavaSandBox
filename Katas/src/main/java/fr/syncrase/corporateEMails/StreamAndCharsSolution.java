package fr.syncrase.corporateEMails;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamAndCharsSolution implements EMailGenerator {
    @Override
    public String buildAddressBook(String names, String company) {

        String[] allNames = names.split(", ");

        Stream<String> myStream;
        myStream = Arrays.asList(allNames).stream();
        myStream = Stream.of(allNames);

        return myStream//
                .map(name -> {
                    String[] splittedName = name.split(" ");
                    char[] onlyFirstLetters = Stream.of(splittedName).map(nameMonade -> nameMonade.substring(0, 1))
                            .collect(Collectors.joining("_")).toCharArray();

                    String nonTruncatedLastName = splittedName[splittedName.length - 1].replace("-", "");
                    char[] onlyLastName = (nonTruncatedLastName.length() > 8 ? nonTruncatedLastName.substring(0, 8)
                            : nonTruncatedLastName).toCharArray();

                    char[] completeEmailId = new char[onlyFirstLetters.length - 1 + onlyLastName.length];
                    for (int i = 0; i < onlyFirstLetters.length - 1; i++) {
                        completeEmailId[i] = onlyFirstLetters[i];
                    }
                    for (int i = onlyFirstLetters.length - 1; i < completeEmailId.length; i++) {
                        completeEmailId[i] = onlyLastName[i - onlyFirstLetters.length + 1];
                    }

                    String[] arrForRes = new String[splittedName.length + 1];
                    for (int i = 0; i < splittedName.length; i++) {
                        arrForRes[i] = splittedName[i];
                    }
                    arrForRes[splittedName.length] = Stream
                            .of(new String[]{
                                    Stream.of(completeEmailId).map(String::valueOf).collect(Collectors.joining()),
                                    company})
                            .map(String::valueOf).collect(Collectors.joining("@", "<", ">")).toLowerCase();

                    return Stream.of(arrForRes).map(String::valueOf).collect(Collectors.joining(" "));

                })//
                .collect(Collectors.joining(", "));

    }
}

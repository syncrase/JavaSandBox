package fr.syncrase.corporateEMails;

public class DurtySolution implements EMailGenerator {

    @Override
    public String buildAddressBook(String names, String company) {

        String[] allNames = names.split(", ");
        StringBuilder sb = new StringBuilder();
        String[] nameSplitted;

        for (int i = 0; i < allNames.length; i++) {
            sb.append(allNames[i]);
            sb.append(" ");
            sb.append("<");

            nameSplitted = allNames[i].split(" ");

            sb.append(nameSplitted[0].toLowerCase().charAt(0));
            sb.append("_");
            String name;

            if (nameSplitted.length == 2) {
                name = nameSplitted[1].replace("-", "");
                name = name.length() > 8 ? name.substring(0, 8) : name;

            } else {

                sb.append(nameSplitted[1].toLowerCase().charAt(0));
                sb.append("_");

                name = nameSplitted[2].replace("-", "");
                name = name.length() > 8 ? name.substring(0, 8) : name;
            }

            sb.append(name.toLowerCase());

            sb.append("@");
            sb.append(company.toLowerCase());
            sb.append(".com");
            sb.append(i == allNames.length - 1 ? ">" : ">, ");

        }

        return sb.toString();
    }
}

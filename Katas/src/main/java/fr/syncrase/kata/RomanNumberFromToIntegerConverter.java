package fr.syncrase.kata;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class RomanNumberFromToIntegerConverter {

    List<Map.Entry<String, Integer>> romanArabMapping;
    SortedMap<String, Integer> romanArabTreeMap;

    List<Map.Entry<Integer, String>> arabRomanMapping;

    public RomanNumberFromToIntegerConverter() {
        SortedMap<Integer, String> arabRomanTreeMap = new TreeMap<>();
        // 10^0
        arabRomanTreeMap.put(1, "I");// 0
        arabRomanTreeMap.put(5, "V");// 1

        // 10^1
        arabRomanTreeMap.put(10, "X");// 2
        arabRomanTreeMap.put(50, "L");// 3

        // 10^2
        arabRomanTreeMap.put(100, "C");// 4
        arabRomanTreeMap.put(500, "D");// 5

        // 10^3
        arabRomanTreeMap.put(1000, "M");// 6
        this.arabRomanMapping = arabRomanTreeMap.entrySet().stream().toList();


        //


        romanArabTreeMap = new TreeMap<>();
        // 10^0
        romanArabTreeMap.put("I", 1);// 0
        romanArabTreeMap.put("V", 5);// 1

        // 10^1
        romanArabTreeMap.put("X", 10);// 2
        romanArabTreeMap.put("L", 50);// 3

        // 10^2
        romanArabTreeMap.put("C", 100);// 4
        romanArabTreeMap.put("D", 500);// 5

        // 10^3
        romanArabTreeMap.put("M", 1000);// 6
        this.romanArabMapping = romanArabTreeMap.entrySet().stream().toList();
    }

    /**
     * Converts the given Arabic notation to Roman notation.
     * <p>
     * Data structure: : 999 => [9, 9, 9]
     * En sortie le tableau est le suivant => [CM, XC, IX]
     * La logique appliquée à chaque rang est la même, seuls les symboles changent en fonction du rang
     *
     * @param arabNotation the Arabic notation to convert
     * @return the Roman notation corresponding to the given Arabic notation
     */
    public String convert(int arabNotation) {
        StringBuilder sb = new StringBuilder();
        int[] valuesOfEachRank = intToArrayOfInts(arabNotation);

        for (int i = 0; i < valuesOfEachRank.length; i++) {
            sb.append(mapArabToRomanValue(valuesOfEachRank[i], valuesOfEachRank.length - i - 1));
        }
        return sb.toString();
    }

    static int[] intToArrayOfInts(int n) {
        int j = 0;
        int len = Integer.toString(n).length();
        int[] arr = new int[len];
        while (n != 0) {
            arr[len - j - 1] = n % 10;
            n = n / 10;
            j++;
        }
        return arr;
    }

    /**
     * Maps the given Arabic symbol at the specified rank to its corresponding Roman value.
     *
     * @param arabSymbolAtThisRank the value of the Arabic symbol at this rank
     * @param rank                 0, 1, 2, ... the rank of the decimal number (10^0, 10^1, 10^2, etc.)
     * @return the Roman value associated with the Arabic symbol at the specified rank
     */
    private String mapArabToRomanValue(int arabSymbolAtThisRank, int rank) {
        StringBuilder sb = new StringBuilder();
        int correspondanceAvecRangNombreRomain = rank * 2;

        String rangDecimalCourant = arabRomanMapping.get(correspondanceAvecRangNombreRomain).getValue();
        if (arabSymbolAtThisRank < 4) {
            sb.append(String.valueOf(rangDecimalCourant).repeat(arabSymbolAtThisRank));
            return sb.toString();
        }

        if (arabSymbolAtThisRank == 4) {
            sb.append(arabRomanMapping.get(correspondanceAvecRangNombreRomain).getValue())
                    .append(arabRomanMapping.get(correspondanceAvecRangNombreRomain + 1).getValue());
            return sb.toString();
        }
        if (arabSymbolAtThisRank == 9) {
            sb.append(rangDecimalCourant)
                    .append(arabRomanMapping.get(correspondanceAvecRangNombreRomain + 2).getValue());
            return sb.toString();
        }
        sb.append(arabRomanMapping.get(correspondanceAvecRangNombreRomain + 1).getValue());
        sb.append(String.valueOf(rangDecimalCourant).repeat(arabSymbolAtThisRank - 5));
        return sb.toString();

    }

    public int convert(String romanNotation) {
        char[] romanNotationCharArray = romanNotation.toCharArray();
        int result = 0;
        // Cette fois-ci je parcours les rangs romains (les lettres) pour les transformer en caractères décimaux
        int letterSuccessionCount = 0;
        for (int i = 0; i < romanNotationCharArray.length; i++) {
            char currentChar = romanNotationCharArray[i];
            int letterIndex = i;
            letterSuccessionCount++;
            // Gestion de l'addition des caractères
            boolean aLaFinDuTableau = letterIndex + letterSuccessionCount > romanNotationCharArray.length - 1;
            if (!aLaFinDuTableau) {

                boolean nextCharIsTheSameOne = currentChar == romanNotationCharArray[letterIndex + 1];
                if (nextCharIsTheSameOne) {
                    // Ici, je compte les lettres
                    while (
                            !aLaFinDuTableau &&
                                    currentChar == romanNotationCharArray[letterIndex + letterSuccessionCount]
                    ) {
                        letterSuccessionCount++;
                        i++;
                        aLaFinDuTableau = letterIndex + letterSuccessionCount > romanNotationCharArray.length - 1;
                    }
                }

                // Si le caractère suivant est d'un rang romain supérieur => je fais une soustraction
                boolean nextCharIsBigger = isBigger(currentChar, romanNotationCharArray[letterIndex + 1]);
                if (nextCharIsBigger) {
                    letterSuccessionCount = romanArabTreeMap.get(String.valueOf(currentChar)) - 1;
                    i++;
                }

                result += letterSuccessionCount * romanArabTreeMap.get(String.valueOf(currentChar));
            }
        }
        return result;
    }

    private boolean isBigger(char reference, char toBeCompared) {
        if (reference == toBeCompared) {
            return false;
        }
        int firstIndex = -1;
        int secondIndex = -1;
        List<Map.Entry<String, Integer>> arabMapping = this.romanArabMapping;
        for (int i = 0; i < arabMapping.size(); i++) {
            Map.Entry<String, Integer> romanArabValues = arabMapping.get(i);
            if (romanArabValues.getKey().contentEquals(String.valueOf(reference))) {
                firstIndex = i;
            }
            if (romanArabValues.getKey().contentEquals(String.valueOf(toBeCompared))) {
                secondIndex = i;
            }
        }

        if (firstIndex == -1 || secondIndex == -1) {
            throw new RuntimeException("Unexpected char passed as argument (%d;%d)".formatted(firstIndex, secondIndex));
        }

        return firstIndex < secondIndex;
    }
}

package fr.syncrase.corporateEMails;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class EMailGeneratorTest<T extends EMailGenerator> {
    private T instance;

    protected abstract T createInstance();

    @BeforeEach
    void setUp() {

        instance = createInstance();
    }

    @Test
    public void testSolution() {
        String names = "John Doe, Peter Parker, Mary Jane Watson-Parker, James Doe, John Elvis Doe, Jane Doe, Penny Parker";
        String company = "Example";
        EMailGenerator s = instance;
        assertEquals(
                "John Doe <j_doe@example.com>, Peter Parker <p_parker@example.com>, Mary Jane Watson-Parker <m_j_watsonpa@example.com>, James Doe <j_doe2@example.com>, John Elvis Doe <j_e_doe@example.com>, Jane Doe <j_doe3@example.com>, Penny Parker <p_parker2@example.com>",
                s.buildAddressBook(names, company)
        );
    }

}
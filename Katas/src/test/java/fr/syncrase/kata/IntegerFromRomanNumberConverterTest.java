package fr.syncrase.kata;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IntegerFromRomanNumberConverterTest {

    @ParameterizedTest
    @CsvSource({
            "I,1",
            "V,5",
            "X,10",
            "L,50",
            "C,100",
            "D,500",
            "M,1000",
    })
    void testSimpleSymbolsMapping(String romanNumber, Integer expectedArabNumber) {
        assertEquals(expectedArabNumber, converter.convert(romanNumber));
    }

    @ParameterizedTest
    @CsvSource({
            "II,2",
//            "VV,5",
            "XX,20",
//            "LL,50",
            "CC,200",
//            "DD,500",
            "MM,2000",
    })
    void testMultipleSymbolsAtBeginningMappingForAddition(String romanNumber, Integer expectedArabNumber) {
        // Suivi du même caractère 0 à n fois
        assertEquals(expectedArabNumber, converter.convert(romanNumber));
    }

    @ParameterizedTest
    @CsvSource({
            "IV,4",
            "IX,9",

            "XL,40",
            "XC,90",

            "CD,400",
            "CM,900",
    })
    void testMultipleSymbolsMappingForSoustraction(String romanNumber, Integer expectedArabNumber) {
        // Suivi d'un caractère d'un rang au dessus ou de deux
        assertEquals(expectedArabNumber, converter.convert(romanNumber));
    }

    RomanNumberFromToIntegerConverter converter = new RomanNumberFromToIntegerConverter();

}

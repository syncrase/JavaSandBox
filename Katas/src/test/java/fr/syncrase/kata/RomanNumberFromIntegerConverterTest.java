package fr.syncrase.kata;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RomanNumberFromIntegerConverterTest {

    RomanNumberFromToIntegerConverter converter = new RomanNumberFromToIntegerConverter();

    @ParameterizedTest
    @CsvSource({
            "1,I",
            "2,II",
            "3,III"
    })
    void test_I_Concatenation(Integer arabNumber, String expectedRomanNumber) {
        assertEquals(expectedRomanNumber, converter.convert(arabNumber));
    }

    @ParameterizedTest
    @CsvSource({
            "10,X",
            "20,XX",
            "30,XXX"
    })
    void test_X_Concatenation(Integer arabNumber, String expectedRomanNumber) {
        assertEquals(expectedRomanNumber, converter.convert(arabNumber));
    }

    @ParameterizedTest
    @CsvSource({
            "1,I",
            "5,V",
            "10,X",
            "50,L",
            "100,C",
            "500,D",
            "1000,M",
    })
    void testSymbolsConversion(Integer arabNumber, String expectedRomanNumber) {
        assertEquals(expectedRomanNumber, converter.convert(arabNumber));
    }

    @ParameterizedTest
    @CsvSource({
            "4,IV",
            "9,IX",
            "19,XIX",
    })
    void testLesCasALaCon(Integer arabNumber, String expectedRomanNumber) {
        assertEquals(expectedRomanNumber, converter.convert(arabNumber));
    }

    @Test
    void testThat_I_IsConvertedTo1() {
        assertEquals(1, converter.convert("I"));
    }

    @ParameterizedTest
    @CsvSource({
            "1900,MCM",
            "999,CMXCIX",
    })
    void testDeTest(Integer arabNumber, String expectedRomanNumber) {
        assertEquals(expectedRomanNumber, converter.convert(arabNumber));
    }

}

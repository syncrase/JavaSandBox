package fr.syncrase;

import java.io.Serializable;

@SuppressWarnings("unused")
public class OuterClass {

	private transient volatile long qsdk;

	/*
	 *******************************************************************************
	 *************************** INNER CLASSES *************************************
	 *******************************************************************************/

	/*
	 * An inner class in a serializable class must also be serializable
	 */
	public class NestedClass extends AClass implements Serializable {

		public void print() {
			System.out.println("Message from nested class");
		}

		@Override
		public void publicAbstractVoidMethod() {

		}
	}

	public static class StaticNestedClass extends AClass implements Serializable {

		@Override
		public void publicAbstractVoidMethod() {

		}
	}
}

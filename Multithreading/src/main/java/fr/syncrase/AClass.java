package fr.syncrase;

public abstract class AClass {

	public final void publicVoidMethod() {
	}

	protected abstract void publicAbstractVoidMethod();

}

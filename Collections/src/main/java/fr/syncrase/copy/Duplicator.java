package fr.syncrase.copy;


import fr.syncrase.models.SimpleClass;

import java.util.Arrays;
import java.util.List;

/**
 * These methods return an entirely new list of elements
 * 
 * @author syncrase
 *
 */
public class Duplicator {

	public SimpleClass[] arrayFromList(List<SimpleClass> list) {
		return list.stream()//
			.map(SimpleClass::new).toList()//
				.toArray(new SimpleClass[0]);
	}

	public SimpleClass[] arrayFromArray(SimpleClass[] array) {
		return Arrays.stream(array).toList()//
				.toArray(new SimpleClass[0]);
	}
}

package fr.syncrase.comparison;

import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;

public class ApplyComparator {

    /**
     * Use the comparator in order to find the min value
     *
     * @param list       list from which the min is returned
     * @param comparator used for the comparison
     * @return the min value based on the comparator logic
     */
    public static <T> T getMinValue(@NotNull List<T> list, Comparator<T> comparator) {
        if (list.isEmpty())
            throw new IllegalArgumentException("Can't find a minimum in an empty list!");
        T lowest = list.get(0);
        for (final T element : list) {
            if (comparator.compare(element, lowest) < 0) {
                lowest = element;
            }
        }
        return lowest;
    }
}

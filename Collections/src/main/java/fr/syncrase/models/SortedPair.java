package fr.syncrase.models;

import org.jetbrains.annotations.NotNull;

/**
 * <T extends Comparable<T>> means that passed values implements Comparable and
 * that they are comparable between them
 * ________________________________________ This class guarantee that passed
 * arguments are sorted on instanciation time
 *
 * @param <T>
 * @author Pierre
 */
public record SortedPair<T extends Comparable<T>>(T smallest, T greatest) {

	public SortedPair(@NotNull T smallest, T greatest) {
		if (smallest.compareTo(greatest) < 0) {
			// Second is greater than smallest
			this.smallest = smallest;
			this.greatest = greatest;
		} else if (smallest.compareTo(greatest) > 0) {
			// smallest is greater than greatest
			this.smallest = greatest;
			this.greatest = smallest;
		} else {
			// param are equals
			this.smallest = smallest;
			this.greatest = smallest;
		}
	}


}

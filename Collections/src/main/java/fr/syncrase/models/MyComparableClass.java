package fr.syncrase.models;

/**
 * Compared by value
 * 
 * @author Pierre
 *
 */
public class MyComparableClass implements Comparable<MyComparableClass> {

	private String label, value;
	private Double theDouble;
	private Integer theInt;

	public MyComparableClass(String label, String value) {
		super();
		this.label = label;
		this.value = value;
	}

	/*
	 * Compare the value field
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MyComparableClass other = (MyComparableClass) obj;
		if (value == null) {
			return other.value == null;
		} else return value.equals(other.value);
	}

	@Override
	public String toString() {
		return label + " | " + value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Double getTheDouble() {
		return theDouble;
	}

	public void setTheDouble(Double theDouble) {
		this.theDouble = theDouble;
	}

	public Integer getTheInt() {
		return theInt;
	}

	public void setTheInt(Integer theInt) {
		this.theInt = theInt;
	}

	/*
	 * this < other => - ; this = other => 0 ; this > other => +
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MyComparableClass other) {
		return value.compareToIgnoreCase(other.getValue());
	}

}

package fr.syncrase.models;

public class ComparableSimpleClass  implements Comparable<ComparableSimpleClass> {

    private String label, value;

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    public ComparableSimpleClass(String label, String value) {
        super();
        this.label = label;
        this.value = value;
    }

    @Override
    public int compareTo(ComparableSimpleClass other) {
        return value.compareToIgnoreCase(other.getValue());
    }
}

package fr.syncrase.arrays;

public class QuickSort {
    private int[] input;

    public void sort(int[] numbers) {
        if (numbers == null || numbers.length == 0) {
            return;
        }
        this.input = numbers;
        int length = numbers.length;
        quickSort(0, length - 1);
    }
    /*
     * This method implements in-place quicksort algorithm recursively.
     */

    private void quickSort(int low, int high) {
        int i = low;
        int j = high;

        // pivot is middle index
        int pivot = input[low + (high - low) / 2];

        // Divide into two arrays
        while (i <= j) {
            // Find the farthest number at LEFT which is GREATER
            while (input[i] < pivot) {
                i++;
            }
            // Find the farthest number at RIGHT which is SMALLER
            while (input[j] > pivot) {
                j--;
            }
            if (i <= j) {
                // Swap founded values
                swap(i, j);
                // Bring indexes closer to the pivot
                i++;
                j--;
            }
        }

        // calls quickSort() method recursively
        if (low < j) {
            quickSort(low, j);
        }

        if (i < high) {
            quickSort(i, high);
        }
    }

    private void swap(int i, int j) {
        int temp = input[i];
        input[i] = input[j];
        input[j] = temp;
    }
}

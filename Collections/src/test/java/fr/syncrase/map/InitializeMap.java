package fr.syncrase.map;

import org.apache.commons.collections4.MapUtils;
import org.junit.jupiter.api.Test;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static java.util.stream.Collectors.toMap;
import static org.junit.jupiter.api.Assertions.*;

public class InitializeMap {

    public static Map<String, Object> mapOne;

    static {
        // Init in static initializer block
        mapOne = new HashMap<>();
        mapOne.put("ar01", "Intro to Map");
        mapOne.put("ar02", "Some article");
    }

    @Test
    void staticInitializer() {
        assertEquals("Intro to Map", InitializeMap.mapOne.get("ar01"));
    }

    @Test
    void initializerBlockOfAnonymousClass() {
        Map<String, Object> m = new HashMap<>() {{
            put("key", 1);
            put("seq", 2);
        }};
        assertEquals(1, m.get("key"));
    }

    @Test
    void mapOf_immutable() {
        Map<String, String> immutableMap = Map.of("a", "b", "c", "d");
        assertEquals("b", immutableMap.get("a"));

        assertThrows(UnsupportedOperationException.class, () -> immutableMap.put("a", "you cannot!"));
    }

    @Test
    void copyOf_immutable() {

        // Immutable copy of existing map
        Map<Object, Object> immutableMap = Map.copyOf(mapOne);

        assertNotSame(immutableMap, mapOne);
        assertThrows(UnsupportedOperationException.class, () -> immutableMap.put("a", "you cannot!"));
    }

    @Test
    void ofEntries_immutable() {
        // this works for any number of elements:
        Map<String, String> immutableMap = Map.ofEntries(entry("a", "b"), entry("c", "d"));
        assertEquals("b", immutableMap.get("a"));
        assertThrows(UnsupportedOperationException.class, () -> immutableMap.put("a", "you cannot!"));
    }

    @Test
    void guavaImmutableMap() {
//		Guava
//		Map<String, Integer> left = ImmutableMap.of("a", 1, "b", 2, "c", 3);
    }

    @Test
    void apachePutAll() {
        // Apache utils
        HashMap<String, String> m = new HashMap<>();
        MapUtils.putAll(m, new Object[]{"My key", "my value"});
        assertEquals("my value", m.get("My key"));
    }

    @Test
    void streamOfSimpleEntry() {
        // With SimpleEntry and toMap
        Map<String, String> m = Stream.of(//
                new AbstractMap.SimpleEntry<>("key1", "value1"), //
                new AbstractMap.SimpleEntry<>("key2", "value2"), //
                new AbstractMap.SimpleEntry<>("key3", "value3"))//
            .collect(toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

        assertEquals("value2", m.get("key2"));
    }

}

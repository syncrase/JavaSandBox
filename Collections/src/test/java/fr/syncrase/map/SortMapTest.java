package fr.syncrase.map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SortMapTest {

    HashMap<Integer, String> map;

    @BeforeEach
    void setUp() {
        map = new HashMap<>();
        map.put(55, "John");
        map.put(22, "Apple");
        map.put(66, "Earl");
        map.put(77, "Pearl");
        map.put(12, "George");
        map.put(6, "Rocky");
    }

    @Test
    void sortByValue() {
        List<Map.Entry<Integer, String>> entries = new ArrayList<>(map.entrySet());
        entries.sort(Map.Entry.comparingByValue());
        Map<Integer, String> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<Integer, String> entry : entries) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        LinkedList<Integer> keys = new LinkedList<>();
        keys.add(22);
        keys.add(66);
        keys.add(12);
        keys.add(55);
        keys.add(77);
        keys.add(6);
        assertArrayEquals(keys.toArray(), sortedMap.keySet().toArray());

        TreeSet<String> values = new TreeSet<>();
        values.add("Apple");
        values.add("Earl");
        values.add("George");
        values.add("John");
        values.add("Pearl");
        values.add("Rocky");
        assertArrayEquals(values.toArray(), sortedMap.values().toArray());
    }

    @Test
    void sortByKey() {
        List<Map.Entry<Integer, String>> entries = new ArrayList<>(map.entrySet());
        entries.sort(Map.Entry.comparingByKey());
        Map<Integer, String> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<Integer, String> entry : entries) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        LinkedList<Integer> keys = new LinkedList<>();
        keys.add(6);
        keys.add(12);
        keys.add(22);
        keys.add(55);
        keys.add(66);
        keys.add(77);
        assertArrayEquals(keys.toArray(), sortedMap.keySet().toArray());

        LinkedList<String> values = new LinkedList<>();
        values.add("Rocky");
        values.add("George");
        values.add("Apple");
        values.add("John");
        values.add("Earl");
        values.add("Pearl");
        assertArrayEquals(values.toArray(), sortedMap.values().toArray());

    }
}

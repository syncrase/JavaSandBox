package fr.syncrase.map;

import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SortedMapTest {

    @Test
    void basicUsage() {

        SortedMap<String, String> map = new TreeMap<>();
        map.put("22", "aze");
        map.put("11", "zer");
        map.put("33", "bvc");
        map.put("44", "xcxv");
        map.put("66", "uyt");
        map.put("55", "rfv");

        SortedMap<String, String> hMap = map.headMap("33");// Strictly before
        assertEquals(2, hMap.size());

        SortedMap<String, String> tMap = map.tailMap("33");// After or equals
        assertEquals(4, tMap.size());

        SortedMap<String, String> sMap = map.subMap("22", "55");// After and strictly before
        assertEquals(3, sMap.size());

    }

    @Test
    void treeMapImpl() {

        SortedMap<String, String> map = new TreeMap<>(Comparator.reverseOrder());
        map.put("22", "aze");
        map.put("11", "zer");
        map.put("33", "bvc");
        map.put("44", "xcxv");
        map.put("66", "uyt");
        map.put("55", "rfv");

        SortedMap<String, String> hMap = map.headMap("33");// Strictly before
        assertEquals(3, hMap.size());

        SortedMap<String, String> tMap = map.tailMap("33");// After or equals
        assertEquals(3, tMap.size());

        SortedMap<String, String> sMap = map.subMap("55", "22");// Because of the reverse order : 55 is before 22
        assertEquals(3, sMap.size());

    }
}

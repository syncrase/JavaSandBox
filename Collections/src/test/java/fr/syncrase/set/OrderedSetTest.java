package fr.syncrase.set;

import fr.syncrase.models.SimpleClassComparator;
import fr.syncrase.models.ComparableSimpleClass;
import fr.syncrase.models.SimpleClass;
import org.junit.jupiter.api.Test;

import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class OrderedSetTest {


    @Test
    void overrideSetComparator() {
        TreeSet<SimpleClass> pojoSet = new TreeSet<>(new SimpleClassComparator());
        pojoSet.add(new SimpleClass("aze", "22"));
        pojoSet.add(new SimpleClass("rty", "11"));
        pojoSet.add(new SimpleClass("bio", "33"));

        String[] expectedLabels = new String[]{"aze", "bio", "rty"};
        assertArrayEquals(expectedLabels, pojoSet.stream().map(SimpleClass::getLabel).toList().toArray());
    }

    @Test
    void overrideSetComparatorWithAnonymousComparator() {
        TreeSet<SimpleClass> pojoSet = new TreeSet<>((o1, o2) -> o1.getLabel().compareToIgnoreCase(o2.getLabel()));
        pojoSet.add(new SimpleClass("aze", "22"));
        pojoSet.add(new SimpleClass("rty", "11"));
        pojoSet.add(new SimpleClass("bio", "33"));

        String[] expectedLabels = new String[]{"aze", "bio", "rty"};
        assertArrayEquals(expectedLabels, pojoSet.stream().map(SimpleClass::getLabel).toList().toArray());
    }

    @Test
    void setOfComparableObjects() {
        TreeSet<ComparableSimpleClass> comparableTreeSet = new TreeSet<>();
        comparableTreeSet.add(new ComparableSimpleClass("aze", "22"));
        comparableTreeSet.add(new ComparableSimpleClass("rty", "11"));
        comparableTreeSet.add(new ComparableSimpleClass("bio", "33"));

        String[] expectedLabels = new String[]{"11", "22", "33"};
        assertArrayEquals(expectedLabels, comparableTreeSet.stream().map(ComparableSimpleClass::getValue).toList().toArray());
    }

}

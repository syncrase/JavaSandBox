package fr.syncrase.functions;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * A Consumer is a functional interface that accepts a single input and returns no output.
 * <p>
 * The accept method is the Single Abstract Method (SAM) which accepts a single argument of type T.
 * Whereas, the other one andThen is a default method used for composition.
 *
 * @see Consumer#accept(Object)
 * @see Consumer#andThen(Consumer)
 */
public class ConsumerTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    Stream<String> cities = Stream.of("Sydney", "Dhaka", "New York", "London");

    @Test
    void unConsumerPrendUnParametreEtNeRetourneRienTest() {

        Consumer<String> printConsumer = System.out::println;
        cities.forEach(printConsumer);

        /*
         * Pourquoi rajouter \r ?
         * Si je ne le met pas, les sauts de ligne correspondent à \n uniquement (des linefeed, alias LF ou \u000a ou (char) 0x0A)
         * Alors que toString() retourne en plus des carriages CR (\u000d ou (char) 0x0D)
         * https://stackoverflow.com/questions/13821578/crlf-into-java-string
         */
        String expected = """
                Sydney\r
                Dhaka\r
                New York\r
                London""";
        assertEquals(expected, outputStreamCaptor.toString()
                .trim());


    }

    @Test
    void consumerSuccessionTest() {
        Consumer<List<String>> upperCaseConsumer = list -> list.replaceAll(String::toUpperCase);
        Consumer<List<String>> printConsumer = list -> list.forEach(System.out::println);

        upperCaseConsumer.andThen(printConsumer).accept(cities.collect(Collectors.toList()));

        String expected = """
                SYDNEY\r
                DHAKA\r
                NEW YORK\r
                LONDON""";
        assertEquals(expected, outputStreamCaptor.toString()
                .trim());
    }
}

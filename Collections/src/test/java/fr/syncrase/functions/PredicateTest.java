package fr.syncrase.functions;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * A Predicate interface represents a boolean-valued-function of an argument.
 * A predicate has a test() method which accepts an argument and returns a boolean value.
 *
 * @see Predicate
 */
public class PredicateTest {
    List<String> names = Arrays.asList("John", "Smith", "Samueal", "Catley", "Sie");

    @Test
    void filterListWithPredicateTest() {
        Predicate<String> nameStartsWithS = str -> str.startsWith("S");

        assertEquals(3, names.stream().filter(nameStartsWithS).toList().size());
    }

    @Nested
    class testPredicateAndComposition {
        Predicate<String> startPredicate = str -> str.startsWith("S");
        Predicate<String> lengthPredicate = str -> str.length() >= 5;
        Predicate<String> doitCommencerParSAvecCinqOuPlusCaracteres = startPredicate.and(lengthPredicate);

        @Test
        void andTest() {
            assertEquals(2, names.stream().filter(doitCommencerParSAvecCinqOuPlusCaracteres).toList().size());
        }


        @Test
        void negationTest() {
            Predicate<String> toutSaufCeQuiCommenceParSAvecCinqOuPlusCaracteres = doitCommencerParSAvecCinqOuPlusCaracteres.negate();
            assertEquals(3, names.stream().filter(toutSaufCeQuiCommenceParSAvecCinqOuPlusCaracteres).toList().size());
        }


        @Test
        void orTest() {
            Predicate<String> soitCommenceParSSoitPossedeCinqOuPlusCaracteres = startPredicate.or(lengthPredicate);
            assertEquals(4, names.stream().filter(soitCommenceParSSoitPossedeCinqOuPlusCaracteres).toList().size());
        }
    }

}

package fr.syncrase.functions;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FunctionTest {

    Function<String, Integer> getLengthFunction = String::length;

    @Test
    public void functionAsStreamMpaInputTest() {
        List<String> names = Arrays.asList("Smith", "Gourav", "Heather", "John", "Catania");
        List<Integer> nameLength = names.stream().map(getLengthFunction).toList();

        int[] ints = new int[]{5, 6, 7, 4, 7};
        for (int i = 0; i < ints.length; i++) {
            int expectedLength = ints[i];
            assertEquals(expectedLength, nameLength.get(i));
        }
    }

    @Test
    public void applyFunctionDirectlyTest() {
        assertEquals(6, getLengthFunction.apply("azeaze"));
    }
}

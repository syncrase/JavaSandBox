package fr.syncrase.functions;


import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.function.DoubleSupplier;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * A SupplierTest is a simple interface which indicates that this implementation is a supplier of results.
 * The supplier has only one method get()
 *
 * @see java.util.function.Supplier
 */
public class SupplierTest {
    @Test
    public void supplier() {
        Supplier<Double> doubleSupplier1 = () -> Double.parseDouble("2");
        DoubleSupplier doubleSupplier2 = () -> Double.parseDouble("3");

        assertEquals(2, doubleSupplier1.get());
        assertEquals(3, doubleSupplier2.getAsDouble());
    }

    @Test
    void supplierAvecOptionalTest() {
        Supplier<Double> mySupplier = () -> Double.parseDouble("2");
        Optional<Double> empty = Optional.empty();

        System.out.println(empty.orElseGet(mySupplier));
    }
}

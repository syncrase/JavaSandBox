package fr.syncrase.arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class QuickSortTest {

        QuickSort algorithm = new QuickSort();

    @Test
    void sort() {
        int[] unsorted = {6, 5, 3, 1, 8, 7, 2, 4};
        algorithm.sort(unsorted);
        System.out.println("Sorted array :" + Arrays.toString(unsorted));
        assertArrayEquals(unsorted, new int[]{1, 2, 3, 4, 5, 6, 7, 8});
    }
}

package fr.syncrase.arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SortArrayTest {

    BubbleSortAlgorithms sortArray = new BubbleSortAlgorithms();

    @Test
    void sortArrayRange() {
        int[] toSort = new int[]{5, 1, 89, 255, 7, 88, 200, 123, 66};
        Arrays.sort(toSort, 3, 7);
        assertArrayEquals(toSort, new int[]{5, 1, 89, 7, 88, 200, 255, 123, 66});
    }

    @Test
    void sortAllArray() {
        int[] toSort = new int[]{5, 1, 89, 255, 7, 88, 200, 123, 66};
        Arrays.sort(toSort);
        assertArrayEquals(toSort, new int[]{1, 5, 7, 66, 88, 89, 123, 200, 255});
    }

    @Test
    void reverseSortAllArray() {
        Object[] toSort = new Object[]{5, 1, 89, 255, 7, 88, 200, 123, 66};
        // The reverse order sort cannot be applied to primitive types
        Arrays.sort(toSort, Collections.reverseOrder());
        assertArrayEquals(toSort, new Object[]{255, 200, 123, 89, 88, 66, 7, 5, 1});
    }

    @Test
    void sortAllArrayWithStream() {
        int[] toSort = new int[]{5, 1, 89, 255, 7, 88, 200, 123, 66};
        int[] sortedArray = Arrays.stream(toSort).sorted().toArray();
        assertArrayEquals(sortedArray, new int[]{1, 5, 7, 66, 88, 89, 123, 200, 255});
    }

    @Test
    void reverseSortAllArrayWithStream() {
        int[] toSort = new int[]{5, 1, 89, 255, 7, 88, 200, 123, 66};
        int[] ints = IntStream.of(toSort)
            .boxed()
            .sorted(Comparator.reverseOrder())
            .mapToInt(i -> i)
            .toArray();
        assertArrayEquals(ints, new int[]{255, 200, 123, 89, 88, 66, 7, 5, 1});
    }

    @Test
    void bubbleSort() {
        int[] toSort = new int[]{5, 1, 89, 255, 7, 88, 200, 123, 66};
        sortArray.bubbleSort(toSort);
        assertArrayEquals(toSort, new int[]{1, 5, 7, 66, 88, 89, 123, 200, 255});
    }
}

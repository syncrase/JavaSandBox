package fr.syncrase.arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class IterateOverArrayTest {
    int[] array;

    @BeforeEach
    void setUp() {
        array = new int[5];
        Arrays.fill(array, 0);
    }

    @Test
    void forLoop() {
        for (int i = 0; i < array.length; i++) {
            assertEquals(0, array[i]);
        }
    }

    @Test
    void enhancedForLoop() {
        for (int j : array) {
            assertEquals(0, j);
        }
    }

    @Test
    void arrayStream() {
        IntStream intStream = Arrays.stream(array);
        assertFalse(intStream
            .map(operand -> operand + 1)
            .filter(value -> value == 0)
            .findFirst()
            .isPresent());
    }

    @Test
    void streamOf() {
        assertEquals(1, Stream.of(array).collect(Collectors.toSet()).size());
    }
}

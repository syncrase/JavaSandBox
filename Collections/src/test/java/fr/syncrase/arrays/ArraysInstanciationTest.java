package fr.syncrase.arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class ArraysInstanciationTest {

    @Test
    void oneAtATime() {
        int[] oneAtATime = new int[5];
        for (int i = 0; i < 5; i++) {
            oneAtATime[i] = i * 2 - 1;
        }
        assertEquals(7, oneAtATime[4]);
        assertEquals(5, oneAtATime.length);
    }

    @Test
    void outOfBound() {
        Exception exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            for (int i = 0; true; i++) {
                (new int[1])[i] = i + 2;
            }
        });
        String expectedMessage = "Index 1 out of bounds for length 1";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void atATimeOfDeclarationWithConstructor() {
        int[] atATimeOfDeclaration = new int[]{5, 1, 89, 255, 7, 88, 200, 123, 66};
        assertEquals(9, atATimeOfDeclaration.length);
        assertEquals(89, atATimeOfDeclaration[2]);
    }

    @Test
    void atATimeOfDeclarationWithArrayInitializer() {
        int[] atATimeOfDeclaration = {5, 1, 89, 255, 7, 88, 200, 123, 66};
        assertEquals(9, atATimeOfDeclaration.length);
        assertEquals(89, atATimeOfDeclaration[2]);
    }

    @Test
    void arraysFillAll() {
        int[] array = new int[5];
        Arrays.fill(array, 30);
        assertArrayEquals(array, new int[]{30, 30, 30, 30, 30});
    }

    @Test
    void arraysFillRange() {
        int[] array = new int[5];
        Arrays.fill(array, 0, 3, -50);
        assertArrayEquals(array, new int[]{-50, -50, -50, 0, 0});
    }

    @Test
    void arraysCopyOf() {
        int[] array = {1, 2, 3, 4, 5};
        int[] copy = Arrays.copyOf(array, 5);
        assertNotSame(array, copy);
        assertArrayEquals(array, copy);
    }

    @Test
    void arraysSetAll() {
        int[] array = new int[20];
        Arrays.setAll(array, p -> p > 9 ? 0 : p);

        assertArrayEquals(array, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
    }

    @Test
    void arrayFromStream() {
        int[] ints = IntStream.range(4, 10).toArray();
        assertArrayEquals(ints, new int[]{4, 5, 6, 7, 8, 9});
    }
}

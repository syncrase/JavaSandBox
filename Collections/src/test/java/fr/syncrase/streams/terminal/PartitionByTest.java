package fr.syncrase.streams.terminal;

import fr.syncrase.models.SimpleClass;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PartitionByTest {

    Stream<SimpleClass> list = Stream.of(
            new SimpleClass("abc", (String) null),
            new SimpleClass("aze", "2"),
            new SimpleClass("abc", "3")
    );

    @Test
    void basicUse() {
        // Partition list based on the field's nullity
        Map<Boolean, List<SimpleClass>> splitOnValueNullity = list.collect(//
                Collectors.partitioningBy(s -> s.getValue() != null)//
        );

        // Je dois avoir deux listes, pour les éléments null et pour les autres
        assertEquals(2, splitOnValueNullity.size());
        // Je dois obtenir une liste de deux éléments non null
        assertEquals(2, splitOnValueNullity.get(true).size());
        // Je dois obtenir une liste d'un élément null
        assertEquals(1, splitOnValueNullity.get(false).size());
    }
}

package fr.syncrase.streams.terminal;

import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringStreamTest {

    Stream<String> stream = Stream.of("t", "e", "s", "t");

    @Test
    void stringReduceAndConcat() {
        assertEquals(
            "test",
            stream.reduce("", String::concat)
        );
    }

    @Test
    void basicConcatenation() {
        assertEquals(
            "t, e, s, t",
            stream.collect(Collectors.joining(", "))
        );
    }

    @Test
    void basicConcatenation_withPrefixAndSuffix() {
        assertEquals(
            "[t, e, s, t]",
            stream.collect(Collectors.joining(", ", "[", "]"))
        );
    }
}

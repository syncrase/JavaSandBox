package fr.syncrase.streams.terminal;

import fr.syncrase.models.SimpleClass;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GroupingByTest {

    Stream<SimpleClass> list = Stream.of(
        new SimpleClass().label("abc").value(null).theDouble(10.0).theInteger(1),
        new SimpleClass().label("aze").value("2").theDouble(17.0).theInteger(2),
        new SimpleClass().label("bouh").value("2").theDouble(17.0).theInteger(2)
    );

    @Test
    void basicUse() {
        Map<Double, List<SimpleClass>> byDouble = list
            .collect(
                groupingBy(SimpleClass::getTheDouble)
            );
        assertEquals(2, byDouble.size());
    }

    @Test
    void groupAndReduce() {
        Collector<SimpleClass, ?, Map<Double, Integer>> reduceGroups = groupingBy(
            SimpleClass::getTheDouble, // Classifier function : mapping input to keys
            summingInt(SimpleClass::getTheInteger)// Implementation of the downstream reduction
        );

        Map<Double, Integer> totalByDept = list.collect(reduceGroups);
        assertEquals(1, totalByDept.get(10.0));
        assertEquals(4, totalByDept.get(17.0));
    }

    @Test
    void groupInCustomCollection() {
        Collector<SimpleClass, ?, Set<String>> groupLabelsInSet = mapping(SimpleClass::getLabel, toSet());

        Collector<SimpleClass, ?, TreeMap<Double, Set<String>>> groupInSetsAllItemsWithTheSameDouble = groupingBy(
            SimpleClass::getTheDouble, // Classifier function
            TreeMap::new, // downstream reduction
            groupLabelsInSet// Map factory
        );

        Map<Double, Set<String>> setOfLabelsByDouble = list.collect(
            groupInSetsAllItemsWithTheSameDouble
        );
        assertEquals(1, setOfLabelsByDouble.get(10.0).size());
        assertEquals(2, setOfLabelsByDouble.get(17.0).size());
    }

    @Test
    void mergeValues() {
        Stream<SimpleClass> list = Stream.of(
            new SimpleClass("abc", "1"),
            new SimpleClass("aze", "2"),
            new SimpleClass("abc", "3")
        );
        // When creating a map where keys are duplicated
        Map<String, Set<String>> map = list.collect(
            Collectors.groupingBy(
                SimpleClass::getLabel,
                Collectors.mapping(
                    SimpleClass::getValue,
                    Collectors.toSet()
                )
            )
        );
        // Keys are associated with a map
        assertEquals(2, map.keySet().size());
        assertArrayEquals(new String[]{"1", "3"}, map.get("abc").toArray());
    }
}

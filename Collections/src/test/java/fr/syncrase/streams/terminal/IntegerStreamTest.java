package fr.syncrase.streams.terminal;

import org.junit.jupiter.api.Test;

import java.util.IntSummaryStatistics;
import java.util.stream.Stream;

import static java.util.stream.Collectors.summarizingInt;
import static org.junit.jupiter.api.Assertions.assertEquals;

class IntegerStreamTest {

    Stream<Integer> ints = Stream.of(1, 2, 3, 4);

    @Test
    void reduceAndSum() {
        assertEquals(
            10,
            ints.reduce(0, Integer::sum)
        );
    }

    @Test
    void mapAndSum() {
        assertEquals(
            10,
            ints.mapToInt(Integer::intValue).sum()
        );
    }

    @Test
    void intReduce() {
        assertEquals(
            1,
            ints.reduce(0, (a, b) -> a / 2 + b / 3)
        );
    }

    @Test
    void summarizingInt_basicUse() {
        IntSummaryStatistics stats = ints.collect(summarizingInt(a -> a));
        assertEquals(2.5, stats.getAverage());
        assertEquals(4, stats.getMax());
        assertEquals(1, stats.getMin());
        assertEquals(10, stats.getSum());
        assertEquals(4, stats.getCount());
    }
}

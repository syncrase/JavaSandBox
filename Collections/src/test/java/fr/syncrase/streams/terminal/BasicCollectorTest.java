package fr.syncrase.streams.terminal;

import fr.syncrase.models.SimpleClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class BasicCollectorTest {

    Stream<String> stream = Stream.of("t", "e", "s", "t");

    @Test
    void toList() {
        List<String> myList = stream.toList();
        assertEquals(4, myList.size());
    }

    @Test
    void toSet() {
        Set<String> mySet = stream.collect(Collectors.toSet());
        assertEquals(3, mySet.size());
    }

    @Test
    void toMap() {
        Set<String> mySet = new HashSet<>();
        mySet.add("test");
        mySet.add("test1");
        mySet.add("test2");

        Map<String, String> stringMap = mySet.stream().collect(Collectors.toMap(
                Function.identity(), // mandatory key mapper
                s -> s // mandatory value mapper
        ));

        stringMap.forEach(Assertions::assertEquals);
    }

    @Test
    void toMap_mergeFunction() {
        List<String> mySet = new ArrayList<>();
        mySet.add("test");
        mySet.add("test1");
        mySet.add("test2");
        mySet.add("test2");

        TreeMap<String, String> stringMap = mySet.stream().collect(
                Collectors.toMap(
                        Function.identity(), // mandatory key mapper
                        s -> s, // mandatory value mapper
                        (oldValue, newValue) -> oldValue + newValue, // optional merge function
                        TreeMap::new// optional supplier
                )
        );

        assertEquals("test2test2", stringMap.get("test2"));
    }

    @Test
    void throwExceptionWhen_multipleValuesAppearsToHaveTheSameKey() {
        Stream<SimpleClass> list = Stream.of(
                new SimpleClass("abc", "1"),
                new SimpleClass("aze", "2"),
                new SimpleClass("abc", "3")
        );
        Exception ex = assertThrows(IllegalStateException.class, (() -> {
            Map<String, SimpleClass> collect = list.collect(
                    Collectors.toMap(
                            SimpleClass::getLabel,
                            Function.identity()
                    )
            );
            fail(collect + "It will never occur");
        }));
        assertTrue(ex.getMessage().contains("Duplicate key abc (attempted merging values abc | 1 and abc | 3)"));
    }

    @Test
    void conversion() {
        Set<String> mySet = stream.collect(Collectors.toSet());
        List<String> myList = mySet.stream().toList();
        assertEquals(3, myList.size());
    }

    @Test
    void explicitImplementation() {
        TreeSet<String> set = stream.collect(Collectors.toCollection(TreeSet::new));
        assertEquals("t", set.last());
    }

    @Test
    void toListReturnUnmodifiableListTest() {
        Consumer<List<String>> upperCaseConsumer = list -> list.replaceAll(String::toUpperCase);
        List<String> testList = stream.toList();

        assertThrows(UnsupportedOperationException.class, () -> upperCaseConsumer.accept(testList));

    }
}

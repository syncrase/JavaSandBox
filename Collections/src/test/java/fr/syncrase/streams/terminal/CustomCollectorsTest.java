package fr.syncrase.streams.terminal;

import fr.syncrase.models.SimpleClass;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class CustomCollectorsTest {

    @Test
    void supplierConstraint_treeSetImplyComparableObjects() {
        Stream<SimpleClass> list = Stream.of(
            new SimpleClass("abc", null, 10.0),
            new SimpleClass("aze", "2", 17.0),
            new SimpleClass("bouh", "2", 17.0)
        );
        // Extract to set
        Collector<SimpleClass, ?, TreeSet<SimpleClass>> intoSet = Collector.of(//
            TreeSet::new, // SupplierTest
            TreeSet::add, // Accumulator
            (left, right) -> {
                throw new UnsupportedOperationException("The combine function is called only when a ReduceTask completes, and ReduceTask instances are used only when evaluating a pipeline in parallel.");
            }
        );
        // With this collector, objects must be comparable
        Exception ex = assertThrows(ClassCastException.class, () -> {
            TreeSet<SimpleClass> setOfLabelsBydoudle2 = list.collect(intoSet);
            fail(setOfLabelsBydoudle2 + "It'll never occur");
        });
        String errorMessage = "class fr.syncrase.models.SimpleClass cannot be cast to class java.lang.Comparable (fr.syncrase.models.SimpleClass is in unnamed module of loader 'app'; java.lang.Comparable is in module java.base of loader 'bootstrap')";
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    void basicUse() {
        List<String> list = Arrays.asList("a", "b", "c", "d", "e");
        StringBuilder collect = list.stream()
            .collect(
                StringBuilder::new,
                StringBuilder::append,
                (a, b) -> {
                    throw new UnsupportedOperationException("The combine function is called only when a ReduceTask completes, and ReduceTask instances are used only when evaluating a pipeline in parallel.");
                }
            );
        assertEquals("abcde", collect.toString());
    }

    @Test
    void combinerUse() {
        List<String> list = Arrays.asList("a", "b", "c", "d", "e");
        StringBuilder collect = list.parallelStream()
            .collect(
                StringBuilder::new,
                StringBuilder::append,
                (a, b) -> a.append("z").append(b)
            );
        assertEquals("azbzczdze", collect.toString());
    }
}

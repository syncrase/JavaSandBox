package fr.syncrase.streams.terminal;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * <a href="https://docs.oracle.com/javase/tutorial/collections/streams/reduction.html#collect">collect doc</a>
 * Unlike the reduce method, which always creates a new value when it processes an element,
 * the collect method modifies, or mutates, an existing value.
 */
public class CollectCombinerTest {

    Stream<String> stringList = new ArrayList<String>() {{
        add("1");
        add("1");
        add("1");
    }}.stream();
    private final DoubleStream doubleStream = stringList.mapToDouble(Double::valueOf);

    @Test
    void notCalledCombinerTest() {
        ArrayList<Integer> collect = doubleStream.collect(
                ArrayList::new,
                // Modify le premier argument en le combinant avec le second
                (s, value) -> {
                    System.out.printf("ACCUMULATE s=%s, value=%s\n", s, value);
                    s.add((int) value);
                },
                (longs, longs2) -> {
                    System.out.printf("COMBINE longs=%s, longs2=%s\n", longs, longs2);
                    if (!longs.containsAll(longs2))
                        longs.addAll(longs2);
                }
        );

        assertEquals(3, collect.size());// La lambda "combiner" n'a pas été utilisée
    }

    @Test
    void calledCombinerTest() {
        // Utilisé pour combiner les différents stream concurrents
        ArrayList<Integer> collect = doubleStream.parallel().collect(
                // Fourni l'objet de base sur lequel l'accumulation sera faite
                ArrayList::new,
                // Modify le premier argument en le combinant avec le second
                (s, value) -> {
                    System.out.printf("ACCUMULATE s=%s, value=%s\n", s, value);
                    s.add((int) value);
                },
                (longs, longs2) -> {
                    System.out.printf("COMBINE longs=%s, longs2=%s\n", longs, longs2);
                    if (!longs.containsAll(longs2))
                        longs.addAll(longs2);
                }
        );

        assertEquals(1, collect.size());// La lambda "combiner" a été utilisée, et aucun élément n'a été rajouté après le premier
    }

    @Test
    void name() {
        Averager averageCollect = Stream.of(1, 2, 3, 4, 5, 6)
                .collect(Averager::supply, Averager::accumulate, Averager::combine);

        assertEquals(3.5, averageCollect.average());

        averageCollect.accumulate(7);
        assertEquals(4, averageCollect.average());
    }

    static class Averager {
        private int total = 0;
        private int count = 0;

        public double average() {
            return count > 0 ? ((double) total) / count : 0;
        }

        public void accumulate(int i) {
            total += i;
            count++;
        }

        /*
        Uniquement utilisé dans le cas où un parallèle stream est utilisé
         */
        public void combine(Averager other) {
            total = 0;
            count = 0;
//            total += other.total;
//            count += other.count;
        }

        public static Averager supply() {
            return new Averager();
        }
    }
}

package fr.syncrase.streams.intermediate;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MapMultiTest {

    @Test
    void testQueLesElementsSontSupprimes() {

        Number mockedNumber = mockNumber();
        Stream<Number> numbers = Stream.of(mockedNumber, mockedNumber, 45);

        List<Integer> integers = numbers.<Integer>mapMulti((number, consumer) -> {
                    if (number instanceof Integer i)
                        consumer.accept(i);
                })
                .toList();
        assertEquals(1, integers.size());
    }

    @NotNull
    private static Number mockNumber() {
        return new Number() {
            @Override
            public int intValue() {
                return 0;
            }

            @Override
            public long longValue() {
                return 0;
            }

            @Override
            public float floatValue() {
                return 0;
            }

            @Override
            public double doubleValue() {
                return 0;
            }
        };
    }

    @Test
    void testLaConversionDUnElementEnPlusieursElements() {
        var nestedList = List.of(
                1,
                List.of(
                        2,
                        List.of(3, 4)
                ),
                5
        );
        assertEquals(3, nestedList.size());

        Stream<Object> expandedStream = nestedList.stream().mapMulti(MapMultiTest::expandIterable);
        assertEquals(5, expandedStream.toList().size());
    }

    static void expandIterable(Object e, Consumer<Object> c) {
        if (e instanceof Iterable<?> elements) {
            for (Object ie : elements) {
                expandIterable(ie, c);
            }
        } else if (e != null) {
            c.accept(e);
        }
    }
}

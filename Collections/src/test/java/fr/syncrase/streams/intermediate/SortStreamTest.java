package fr.syncrase.streams.intermediate;

import fr.syncrase.models.SimpleClass;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SortStreamTest {

    @Test
    void basicSort() {

        List<SimpleClass> people = new ArrayList<>() {{
            add(new SimpleClass("nom1", 5000));
            add(new SimpleClass("nom2", 15000));
        }};

        List<String> list = people.stream()
            .sorted(
                Comparator.comparing(SimpleClass::getLabel).reversed()
            )
            .map(SimpleClass::getLabel)
            .toList();

        assertEquals("nom2", list.get(0));
    }

}

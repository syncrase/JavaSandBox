
/**
 * Un Stream est obtenu à partir d'une source (une liste, un set, ...)
 * Suivi de 0 ou plus opérations intermédiaire (filter, map)
 * Et se termine par un opérateur terminal (forEach, reduce)
 * <p>
 * <p>
 * Ecosystème objet de l'API Stream
 * <ul>
 *     <li>Stream</li>
 *     <li>Predicate</li>
 *     <li>SupplierTest</li>
 *     <li>Consumer</li>
 *     <li>Function</li>
 *     <li>BiConsumer</li>
 *     <li>StreamSupport</li>
 *     <li></li>
 * <p>
 * <p>
 *     Les sources d'un stream sont multiples
 *     <ul>
 *         <li>Stream.empty()</li>
 *         <li>Stream.of(T... values) ajoute toutes les valeurs</li>
 *         <li>Stream.ofNullable(T value) n'joute pas les éléments null</li>
 *         <li>x2 iterate(seed, function) stream séquentiel infini est ordonné</li>
 *         <li>generate(supplier) stream séquentiel infini</li>
 *         <li>concat</li>
 *         <li>Utilisation du builder</li>
 *     </ul>
 * <p>
 *     Les opérateurs intermédiaires
 *     <li>flatMap</li>
 *     <li>map</li>
 *     <li>mapMulti</li>
 *     <li>takeWhile</li>
 *     <li>dropWhile</li>
 *     <li>filter</li>
 *     <li>distinct</li>
 *     <li>sorted x2/li>
 *     <li>peek</li>
 *     <li>limit</li>
 *     <li>skip</li>
 * <p>
 *     Les opérateurs terminaux
 *     <li>toArray x2</li>
 *     <li>reduce x3</li>
 *     <li>collect x2</li>
 *     <li>forEach</li>
 *     <li>forEachOrdered</li>
 *     <li>toList</li>
 *     <li>min</li>
 *     <li>max</li>
 *     <li>count</li>
 *     <li>anyMatch</li>
 *     <li>allMatch</li>
 *     <li>noneMatch</li>
 *     <li>findFirst</li>
 * <p>
 *     Les court-circuit terminaux
 *     <li>findAny</li>
 *
 * </ul>
 * <p>
 * Integer
 * <ul>
 *     <li>IntStream</li>
 *     <li>ToIntFunction</li>
 *     <li>IntSupplier</li>
 *     <li></li>
 *
 *     <li>flatMapToInt</li>
 *     <li>mapToInt</li>
 *     <li>mapMultiToInt</li>
 * </ul>
 * <p>
 * Double
 * <ul>
 *     Les types
 *     <li>DoubleStream</li>
 *     <li>ToDoubleFunction</li>
 *     <li>DoubleSupplier</li>
 *     <li></li>
 * <p>
 *     Et les méthodes
 *     <li>flatMapToDouble</li>
 *     <li>mapToDouble</li>
 *     <li>mapMultiToDouble</li>
 * </ul>
 * <p>
 * Long
 * <ul>
 *     <li>LongStream</li>
 *     <li>ToLongFunction</li>
 *     <li>LongSupplier</li>
 *     <li></li>
 * <p>
 *     Et les méthodes
 *     <li>flatMapToLong</li>
 *     <li>mapToLong</li>
 *     <li>mapMultiToLong</li>
 * </ul>
 */
package fr.syncrase.streams;

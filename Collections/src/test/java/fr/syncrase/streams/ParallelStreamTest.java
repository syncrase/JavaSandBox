package fr.syncrase.streams;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;


public class ParallelStreamTest {

    @Test
    void performanceTest() {


        int upperbound = 1000000;

        List<Customer> customerList = new ArrayList<>();

        for (int i = 0; i < upperbound; i++) {
            int numeroClient = i + 1;
            int creance = new Random().nextInt(upperbound);
            customerList.add(new Customer(numeroClient, creance));
        }

        Stream<Customer> customerStream = customerList.stream();
        calculTempsTraitement(customerStream, "Stream");

        Stream<Customer> customerParallelStream = customerList.parallelStream();
        calculTempsTraitement(customerParallelStream, "Parallel Stream");

    }

    private static void calculTempsTraitement(@NotNull Stream<Customer> customerParallelStream, String whatIsCounted) {

        Timer chrono = new Timer();
        System.out.println(whatIsCounted + " count: " + customerParallelStream.filter(c -> c.getReceivables() > 25000).count());
        long dureeDExecution = chrono.duree();

        System.out.println(whatIsCounted + " Time taken:" + dureeDExecution);

    }

}

class Timer {

    Long debut;
    Long fin;

    public Timer() {
        this.demarrer();
    }

    void demarrer() {
        debut = System.currentTimeMillis();
    }

    void arreter() {
        fin = System.currentTimeMillis();
    }

//    long duree() {// YAGNI
//        return duree(ACTION.CONTINUE);
//    }

    long duree() {
//    long duree(ACTION action) {
//        if (action == ACTION.STOP) {
        arreter();
//        }
        // double is a primitive type, it cannot be null, instead you can use Double and then compare it to null
        if (debut == null || fin == null) {
            return -1;
        }
        return fin - debut;
    }
}


class Customer {

    int customerNumber;
    int receivables;

    Customer(int customerNumber, int receivables) {
        this.customerNumber = customerNumber;
        this.receivables = receivables;
    }

    public int getReceivables() {
        return receivables;
    }

}

package fr.syncrase.streams.examples;

import fr.syncrase.models.SimpleClass;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RemoveDoublonsTest {

    Supplier<Stream<SimpleClass>> list = () -> Stream.of(
        new SimpleClass("abc", "80", 10.0),
        new SimpleClass("aze", "2", 17.0),
        new SimpleClass("bouh", "2", 17.0)
    );

    @Test
    void groupingWay() {
        // First step grouping by the criteria that implies object are same
        Map<String, List<SimpleClass>> plantesMap = list.get().collect(Collectors.groupingBy(SimpleClass::getValue));
        // Then, keep only one object of each resulted list of same object
        List<SimpleClass> listWithoutDuplicate = plantesMap.values().stream().map(simpleClasses -> simpleClasses.get(0)).toList();

        // First and second item has been switched because Map order entries by key
        assertEquals("aze", listWithoutDuplicate.get(0).getLabel());
        assertEquals("abc", listWithoutDuplicate.get(1).getLabel());
    }

    @Test
    void setWay() {

        List<SimpleClass> simpleClasses = list.get()
            .collect(
                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(SimpleClass::getValue)))
            )
            .stream()
            .toList();

        // First and second item has been switched because TreeSet order entries by value
        assertEquals("aze", simpleClasses.get(0).getLabel());
        assertEquals("abc", simpleClasses.get(1).getLabel());
    }
}

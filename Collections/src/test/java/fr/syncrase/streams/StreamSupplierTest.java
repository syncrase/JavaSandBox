package fr.syncrase.streams;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class StreamSupplierTest {

    @Test
    void aStreamCanBeCalledOnlyOnce() {
        Stream<String> stringStream = Stream.of("A", "B", "C", "D");
        String firstAndLastCall = stringStream.findAny().orElse("qsd");
        assertEquals("A", firstAndLastCall);
        assertThrows(IllegalStateException.class, () -> {
            String willNeverBeAssigned = stringStream.findAny().orElse("qsd");
            fail(willNeverBeAssigned);
        });
    }

    @Test
    void aStreamSupplierProviderAsManyStreamsAsNecessary() {
        Supplier<Stream<String>> streamSupplier
                = () -> Stream.of("A", "B", "C", "D");

        String firstCall = streamSupplier.get().findAny().orElse("qsd");
        assertEquals("A", firstCall);

        String secondCall = streamSupplier.get().findAny().orElse("qsd");
        assertEquals("A", secondCall);
    }

    @Test
    void intStreamSupplier() {
        int size = 5;
        List<Integer> integerList = IntStream.range(0, size).boxed().toList();
        assertEquals(size, integerList.size());
    }

    /**
     * known subclasses
     * <a href="https://docs.oracle.com/javase/7/docs/api/java/io/Reader.html">...</a>
     */
    @Test
    void readInputStream() throws IOException {
        // See all the direct known subclasses in order to see all objects available for
        // that
        InputStream stream = new ByteArrayInputStream("myString".getBytes());
        int letter;

        SimpleQueue simpleQueue = new SimpleQueue("109", "121", "83", "116", "114", "105", "110", "103");
        while ((letter = stream.read()) > -1) {
            byte byteVal = (byte) letter;
            // Do something with byteVal
            assertEquals(simpleQueue.get(), Byte.valueOf(byteVal));
        }

    }
    @Test
    void readWithByteArrayInputStream() throws IOException {
        // The way to read an array of bytes
        int numberOfBytesRead;
        byte[] byteBuff = new byte[10];
        SimpleQueue expected = new SimpleQueue("109", "121", "83", "116", "114", "105", "110", "103");
        var qsd = new ByteArrayInputStream("myString".getBytes());
        while ((numberOfBytesRead = qsd.read(byteBuff)) > -1) {
            for (int i = 0; i < numberOfBytesRead; i++) {
                // Get values byte by byte
                byte byteVal = byteBuff[i];
                // Use this
                assertEquals(expected.get(), Byte.valueOf(byteVal));
            }
        }
    }

}

class SimpleQueue {

    private final List<Byte> byteList;
    private int index;

    public SimpleQueue(String... args) {
        byteList = Arrays.stream(args).map(Byte::parseByte).toList();
        index = 0;
    }

    Byte get() {
        return byteList.get(index++);
    }
}

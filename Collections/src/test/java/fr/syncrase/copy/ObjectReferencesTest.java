package fr.syncrase.copy;

import fr.syncrase.models.SimpleClass;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ObjectReferencesTest {

    @Test
    void sharedObjects() {
        ArrayList<SimpleClass> list = new ArrayList<>();
        list.add(new SimpleClass("abc", "1"));
        list.add(new SimpleClass("aze", "2"));
        list.add(new SimpleClass("abc", "3"));

        SimpleClass[] tableau = list.toArray(SimpleClass[]::new);

        // Modification des valeurs du tableau résultant
        for (SimpleClass msc : tableau) {
            msc.setLabel(msc.getLabel() + msc.getLabel());
        }

        for (int i = 0, listSize = list.size(); i < listSize; i++) {
            assertSame(list.get(i), tableau[i]);
            assertEquals(6, list.get(i).getLabel().length());
        }
    }

    @Test
    void backedCollection() {
        ArrayList<SimpleClass> list = new ArrayList<>();
        list.add(new SimpleClass("abc", "1"));
        list.add(new SimpleClass("aze", "2"));
        list.add(new SimpleClass("abc", "3"));

        Set<SimpleClass> set = new HashSet<>(list);

        // Modification des valeurs du tableau résultant
        for (SimpleClass msc : set) {
            msc.setLabel(msc.getLabel() + msc.getLabel());
        }

        for (SimpleClass simpleClass : list) {
            assertEquals(6, simpleClass.getLabel().length());
        }
    }

    @Test
    void cloneCollection() {
        ArrayList<SimpleClass> list = new ArrayList<>();
        list.add(new SimpleClass("abc", "1"));
        list.add(new SimpleClass("aze", "2"));
        list.add(new SimpleClass("abc", "3"));
        ArrayList<SimpleClass> copiedList = (ArrayList<SimpleClass>) list.clone();

        copiedList.get(0).setValue("8");
        assertEquals("8", list.get(0).getValue());
        assertEquals("8", copiedList.get(0).getValue());

        copiedList.set(0, new SimpleClass("a", "1"));
        assertEquals("8", list.get(0).getValue());
        assertEquals("1", copiedList.get(0).getValue());
    }

    @Test
    void constructorCopy() {
        ArrayList<SimpleClass> list = new ArrayList<>();
        list.add(new SimpleClass("abc", "1"));
        list.add(new SimpleClass("aze", "2"));
        list.add(new SimpleClass("abc", "3"));
        ArrayList<SimpleClass> copiedList = new ArrayList<>(list);

        assertSame(list.get(0), copiedList.get(0));

        copiedList.get(0).setValue("8");
        assertEquals("8", list.get(0).getValue());
        assertEquals("8", copiedList.get(0).getValue());

        copiedList.set(0, new SimpleClass("a", "1"));
        assertNotSame(list.get(0), copiedList.get(0));
        assertEquals("8", list.get(0).getValue());
        assertEquals("1", copiedList.get(0).getValue());
    }

}

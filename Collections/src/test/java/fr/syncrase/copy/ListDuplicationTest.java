package fr.syncrase.copy;

import fr.syncrase.models.SimpleClass;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ListDuplicationTest {


    @Test
    void unmodifiableList() {
        ArrayList<SimpleClass> list = new ArrayList<>();
        list.add(new SimpleClass("abc", "1"));
        list.add(new SimpleClass("aze", "2"));
        list.add(new SimpleClass("abc", "3"));

        List<SimpleClass> lList = List.copyOf(list); // Unmodifiable

        assertThrows(UnsupportedOperationException.class, () -> {
            lList.add(new SimpleClass("abc", "1"));
        });
    }

    @Test
    void unmodifiableSet() {
        Set<SimpleClass> list = new HashSet<>();
        list.add(new SimpleClass("abc", "1"));
        list.add(new SimpleClass("aze", "2"));
        list.add(new SimpleClass("abc", "3"));

        Set<SimpleClass> targetSet = Set.copyOf(list);// Inmodifiable

        assertThrows(UnsupportedOperationException.class, () -> {
            targetSet.add(new SimpleClass("abc", "1"));
        });
    }


}

package fr.syncrase.copy;

import fr.syncrase.models.SimpleClass;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

public class DeepCopyTest {

    @Test
    void deepCopy() {

        // WHEN I create a list
        ArrayList<SimpleClass> list = new ArrayList<>();
        list.add(new SimpleClass("abc", "1"));
        list.add(new SimpleClass("aze", "2"));
        list.add(new SimpleClass("abc", "3"));

        // AND make a deep copy of it
        SimpleClass[] tableau;
        Duplicator duplicator = new Duplicator();
        tableau = duplicator.arrayFromList(list);

        // THEN all references are different
        for (int i = 0; i < tableau.length; i++) {
            assertNotSame(tableau[i], list.get(i));
        }

        // AND modify element
        for (SimpleClass msc : tableau) {
            msc.setLabel(msc.getLabel() + msc.getLabel());
        }

        // THEN values are updated in only one list
        for (int i = 0; i < tableau.length; i++) {
            assertNotEquals(tableau[i].getLabel(), list.get(i).getLabel());
        }

    }
}

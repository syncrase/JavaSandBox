package fr.syncrase.collections;

import fr.syncrase.models.*;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class CollectionsSortTest {

    @Test
    void sortListWithCustomComparator() {
        List<SimpleClass> list2 = new ArrayList<>();
        list2.add(new SimpleClass("aaa", "22"));
        list2.add(new SimpleClass("rrr", "11"));
        list2.add(new SimpleClass("ccc", "33"));

        // Sorted list
        list2.sort(new SimpleClassComparator());
        String[] expectedLabels = new String[]{"aaa", "ccc", "rrr"};
        assertArrayEquals(expectedLabels, list2.stream().map(SimpleClass::getLabel).toList().toArray());

        // Reverse sorted list
        list2.sort(new ReverseComparator<>(new SimpleClassComparator()));
        expectedLabels = new String[]{"rrr", "ccc", "aaa"};
        assertArrayEquals(expectedLabels, list2.stream().map(SimpleClass::getLabel).toList().toArray());
    }

    @Test
    void sortWithAnonymousComparator() {
        List<SimpleClass> list2 = new ArrayList<>();
        list2.add(new SimpleClass("aaa", "22"));
        list2.add(new SimpleClass("rrr", "11"));
        list2.add(new SimpleClass("ccc", "33"));

        // Sorted list
        list2.sort(Comparator.comparing(SimpleClass::getLabel));
        String[] expectedLabels = new String[]{"aaa", "ccc", "rrr"};
        assertArrayEquals(expectedLabels, list2.stream().map(SimpleClass::getLabel).toList().toArray());

        // Reverse sorted list
        list2.sort((a, b) -> b.getLabel().compareTo(a.getLabel()));
        expectedLabels = new String[]{"rrr", "ccc", "aaa"};
        assertArrayEquals(expectedLabels, list2.stream().map(SimpleClass::getLabel).toList().toArray());
    }

    @Test
    void sortListWithEmbeddedComparator() {
        List<ComparableSimpleClass> list2 = new ArrayList<>();
        list2.add(new ComparableSimpleClass("aaa", "22"));
        list2.add(new ComparableSimpleClass("rrr", "11"));
        list2.add(new ComparableSimpleClass("ccc", "33"));

        // Sorted list
        Collections.sort(list2);
        String[] expectedLabels = new String[]{"rrr", "aaa", "ccc"};
        assertArrayEquals(expectedLabels, list2.stream().map(ComparableSimpleClass::getLabel).toList().toArray());

    }

    @Test
    void usingTheComparatorBuilder() {

        List<SimpleClass> list2 = new ArrayList<>();
        list2.add(new SimpleClass("aaa", "22"));
        list2.add(new SimpleClass("rrr", "11"));
        list2.add(new SimpleClass("ccc", "33"));
        list2.add(new SimpleClass(null, "88"));

        // Definition of the sort logic with the Comparator builder
        list2.sort(Comparator//
                .comparing(//
                        SimpleClass::getValue, // by field
                        Comparator.nullsLast(Comparator.naturalOrder())// null-friendly natural order comparator
                )//
                .thenComparing(//
                        SimpleClass::getTheDouble, // by Double field
                        (a, b) -> Double.compare(b, a)// Lambda which return comparator
                )// by Double field
                .thenComparingDouble(SimpleClass::getTheDouble)//// by Double comparison on Double field
                .reversed()//
        );

        String[] expectedLabels = new String[]{null, "ccc", "aaa", "rrr"};
        assertArrayEquals(expectedLabels, list2.stream().map(SimpleClass::getLabel).toList().toArray());
    }

    @Test
    void providedComparators() {
        List<MyComparableClass> list2 = new ArrayList<>();
        list2.add(new MyComparableClass("aaa", "22"));
        list2.add(new MyComparableClass("rrr", "11"));
        list2.add(new MyComparableClass("ccc", "33"));

        list2.sort(Comparator.naturalOrder());
        String[] expectedLabels = new String[]{"rrr", "aaa", "ccc"};
        assertArrayEquals(expectedLabels, list2.stream().map(MyComparableClass::getLabel).toList().toArray());

        list2.sort(Comparator.reverseOrder());
        expectedLabels = new String[]{"ccc", "aaa", "rrr"};
        assertArrayEquals(expectedLabels, list2.stream().map(MyComparableClass::getLabel).toList().toArray());
    }

    @Test
    void providedSpecificComparators() {
        List<Float> testList = Arrays.asList(0.5F, 0.5F, 0.2F, 0.9F, 0.1F, 0.1F, 0.1F, 0.54F, 0.71F, 0.71F, 0.71F,
                0.92F, 0.12F, 0.65F, 0.34F, 0.62F);
        testList.sort((a, b) -> Float.compare(b, a));
        Float[] expectedFloats = new Float[]{0.92F, 0.9F, 0.71F, 0.71F, 0.71F, 0.65F, 0.62F, 0.54F, 0.5F, 0.5F, 0.34F, 0.2F, 0.12F, 0.1F, 0.1F, 0.1F};
        assertArrayEquals(expectedFloats, testList.toArray());

        List<Integer> intList = Arrays.asList(5, 2, 9, 1, 1, 1, 54, 71, 71, 71, 92, 12, 65, 34, 62);
        intList.sort((a, b) -> Integer.compare(b, a));
        Integer[] expectedInts = new Integer[]{92, 71, 71, 71, 65, 62, 54, 34, 12, 9, 5, 2, 1, 1, 1};
        assertArrayEquals(expectedInts, intList.toArray());

    }
}

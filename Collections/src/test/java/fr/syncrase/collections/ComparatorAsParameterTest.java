package fr.syncrase.collections;

import fr.syncrase.models.MyComparableClass;
import fr.syncrase.models.ReverseComparator;
import fr.syncrase.models.SimpleClass;
import fr.syncrase.models.SimpleClassComparator;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static fr.syncrase.comparison.ApplyComparator.getMinValue;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ComparatorAsParameterTest {

    @Test
    void usingProvidedComparator() {
        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);
        assertEquals(1, getMinValue(integers, Integer::compare));
        assertEquals(3, getMinValue(integers, new ReverseComparator<>(Integer::compare)));
    }

    @Test
    void usingComparableObjects() {
        List<MyComparableClass> comparableList = new ArrayList<>();
        comparableList.add(new MyComparableClass("aze", "22"));
        MyComparableClass rty = new MyComparableClass("rty", "11");
        comparableList.add(rty);
        comparableList.add(new MyComparableClass("bio", "33"));

        assertEquals(rty, getMinValue(comparableList, MyComparableClass::compareTo));
    }

    @Test
    void usingCustomComparator() {
        List<SimpleClass> list2 = new ArrayList<>();
        SimpleClass minLabel = new SimpleClass("aaa", "22");
        list2.add(minLabel);
        SimpleClass maxLabel = new SimpleClass("rrr", "11");
        list2.add(maxLabel);
        list2.add(new SimpleClass("ccc", "33"));

        assertEquals(minLabel, getMinValue(list2, new SimpleClassComparator()));
        assertEquals(maxLabel, getMinValue(list2, new ReverseComparator<>(new SimpleClassComparator())));
    }

    @Test
    void usingAnAnonymousComparator() {
        List<SimpleClass> list2 = new ArrayList<>();
        list2.add(new SimpleClass("aaa", "22"));
        list2.add(new SimpleClass("rrr", "11"));
        list2.add(new SimpleClass("ccc", "33"));

        SimpleClass objWithSmallestValue = getMinValue(list2, (o1, o2) -> o1.getLabel().compareToIgnoreCase(o2.getLabel()));
        assertEquals("aaa", objWithSmallestValue.getLabel());
    }
}

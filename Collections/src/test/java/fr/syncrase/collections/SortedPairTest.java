package fr.syncrase.collections;

import fr.syncrase.models.MyComparableClass;
import fr.syncrase.models.SortedPair;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SortedPairTest {

    @Test
    void testPairOrdering() {
        MyComparableClass greatest = new MyComparableClass("z", "z");
        MyComparableClass smallest = new MyComparableClass("c", "d");
        SortedPair<MyComparableClass> s = new SortedPair<>(greatest, smallest);

        assertEquals(greatest, s.greatest());
        assertEquals(smallest, s.smallest());
    }
}

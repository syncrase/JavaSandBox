package fr.syncrase;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * <a href="https://openjdk.org/jeps/406">Pattern matching JEP 406</a>
 * <a href="https://docs.oracle.com/en/java/javase/17/language/pattern-matching.html#GUID-A59EF0C7-4CB7-4555-986D-0FD804555C25">java change</a>
 */
public class PatternMatchingTest {

    @Nested
    class AreaForMultipleShapeTest {
        Shape shape1 = new Rectangle(2, 3);
        Shape shape2 = new Triangle(2, 3);

        @Test
        void rectangleAreaTest() {
            assertEquals(3, computeArea(shape2));
        }

        @Test
        void triangleAreaTest() {
            assertEquals(6, computeArea(shape1));
        }

    }

    private static double computeArea(Shape shape) {
        return switch (shape) {
//            case Rectangle rect && rect.getLargeur() == 0  -> rect.getLargeur() * rect.getLongueur();
            case Rectangle rect -> rect.getLargeur() * rect.getLongueur();
            case Triangle tri -> tri.getBase() * tri.getHauteur() / 2;
            case null -> throw new IllegalStateException("Unexpected value: " + shape);
            default -> 0d;
        };
    }

//    static void testTriangle(Shape s) {
//        switch (s) {
    // Pas dispo en java 21
//            case Triangle t &&(computeArea(t) > 100) ->
//                System.out.println("Large triangle");
//                case Triangle t -> System.out.println("Small triangle");
//                default -> System.out.println("Non-triangle");
//        }
//    }

    record Point(double x, double y) {
    }

    @Test
    @DisplayName("Test de la fonctionnalité d'un record pattern au sein d'une boucle for")
    void patternMatchingRecordTest() {
//        List<Point> points = new ArrayList<>();
//        points.add(new Point(2, 3));

        // Not supported java 21
//        for (Point(double x, double y) : points) {
//            assertEquals(2, x);
//            assertEquals(3, y);
//        }
        fail("En attente de java 22; sinon mettre le 21 en preview");
    }

    @Test
    @DisplayName("Test de la fonctionnalité de déstructuration d'un record au sein d'un instanceof")
    void patternMatchingWithRecordDestructurationTest() {
        List<Point> points = new ArrayList<>();
        points.add(new Point(2, 3));

        if (points.getFirst() instanceof Point(double largeur, double longueur)) {
            assertEquals(2, largeur);
            assertEquals(3, longueur);
        }
    }
}

/**
 * Sealed class : toutes les class permissent doivent être final
 */
sealed class Shape permits Rectangle, Triangle {

}

final class Rectangle extends Shape {
    private final double largeur;
    private final double longueur;

    public Rectangle(double largeur, double longueur) {
        this.largeur = largeur;
        this.longueur = longueur;
    }

    public double getLargeur() {
        return largeur;
    }

    public double getLongueur() {
        return longueur;
    }


}

final class Triangle extends Shape {
    private final double base;
    private final double hauteur;

    public Triangle(double base, double hauteur) {
        this.base = base;
        this.hauteur = hauteur;
    }

    public double getHauteur() {
        return hauteur;
    }

    public double getBase() {
        return base;
    }
}

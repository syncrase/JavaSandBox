package fr.syncrase.primitives;

import org.junit.jupiter.api.Test;

class FloatsTest {

    @Test
    void playWithFloats() {
        System.out.println("float > single-precision 32-bit IEEE 754 floating point");
        System.out.println("(float) 2^31 - 1 -> " + (float) (Math.pow(2, 31) - 1));
        System.out.println("(float) 2^31 -> " + (float) Math.pow(2, 31));// -32768
    }

}

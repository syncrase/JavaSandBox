package fr.syncrase.primitives;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DoublesTest {

    @Test
    void playWithDoubles() {
        System.out.println("double > ");
        System.out.println("(double) 2^31 - 1 -> " + (double) (Math.pow(2, 31) - 1));
        System.out.println("(double) 2^31 -> " + (double) Math.pow(2, 31));// -32768
    }

}

package fr.syncrase.primitives;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharsTest {

    @Test
     void displayTheAllChars() {
        char lastValue = 0;
        for (char c = 0;; c++) {
            System.out.println(((int) c) + " : " + c);
            if ((int) c == 0 && (int) lastValue == 0 || (int) c == (int) lastValue + 1) {
                lastValue = c;
            } else {
                System.out.println("Je viens de passer la valeur max");

                System.out.println("\t- byte min = " + c + " (" + Chars.getBinaryRepresentation(c) + ")");
                System.out.println("\t- byte max = " + lastValue + " (" + Chars.getBinaryRepresentation(lastValue) + ")");
                System.out.println("Nombre de valeurs encodées : max - min + 1 (le 0!) = " + (lastValue - c + 1));
                break;
            }
        }
    }

    @Test
   void displayKnownCharacters() {
        // Display the characters table
        for (long i = 33; i < 127; i++) {
            System.out.println(" |" + i + "\t|" + (char) i + "\t|");
        }
        System.out.println(" \t (...) ");
        for (long i = 161; i < 256; i++) {
            System.out.println(" |" + i + "\t|" + (char) i + "\t|");
        }
    }
}

package fr.syncrase.enumeration;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MethodsEnumTest {

    @Test
    void callEnumerateMethod() {
        assertEquals("item1 method1", MethodsEnum.SET_OF_METHODS.method());
        assertEquals("item1 method2", MethodsEnum.SET_OF_METHODS.method2());
        assertEquals("item2 method1", MethodsEnum.ANOTHER_SET_OF_METHODS.method());
        assertEquals("item2 method2", MethodsEnum.ANOTHER_SET_OF_METHODS.method2());

        assertEquals("ANOTHER_SET_OF_METHODS", MethodsEnum.ANOTHER_SET_OF_METHODS.name());
    }
}

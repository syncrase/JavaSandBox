package fr.syncrase.enumeration;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class CompleteEnumTest {

    @Test
    void getValueByContent() {
        assertArrayEquals(new String[]{"DRY", "WET", "SNOW"}, Arrays.stream(CompleteEnum.values()).map(Enum::name).toArray());

        // Test the label getter
        assertEquals(CompleteEnum.SNOW, CompleteEnum.valueOfDesc("enneigé"));
        assertEquals(CompleteEnum.DRY, CompleteEnum.valueOfCoef(1.0));
        assertEquals(CompleteEnum.DRY, CompleteEnum.valueOf("DRY"));
    }
}

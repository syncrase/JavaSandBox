package fr.syncrase.enumeration;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BasicEnumTest {

    @Test
    void basicUsageOfEnum() {

        assertEquals("P1", BasicEnum.P1.name());
        assertEquals("P1", BasicEnum.valueOf("P1").name());

        assertEquals(BasicEnum.P1, BasicEnum.valueOf("P1"));

        assertArrayEquals(new String[]{"P1", "P2"}, BasicEnum.values());

        assertEquals(0, BasicEnum.valueOf("P1").ordinal());
    }
}

package fr.syncrase.classes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AnonymousClassTest {

    @Test
    void basicUse() {
        String s = new Object() {
            public String getMyString() {
                return "String";
            }
        }.getMyString();

        assertEquals("String", s);
    }

    @Test
    void ghostMethod() {

        Object obj = new Object() {
            public static final int publicStaticFinal = 456;
            public final int publicInt = 5;
            private final int privateInt = 14000;

            public String ghostMethod() {
                return "String";
            }
        };

        // Reflexion way to access
        try {
            Class<?> myClass = obj.getClass();
            // only public access
            Object ghostMethod = myClass.getMethod("ghostMethod").invoke(obj);
            assertEquals("String", ghostMethod);
            int publicStaticFinal = myClass.getField("publicStaticFinal").getInt(obj);
            assertEquals(456, publicStaticFinal);
            int publicInt = myClass.getField("publicInt").getInt(obj);
            assertEquals(5, publicInt);

            // all access
            int privateInt = myClass.getDeclaredField("privateInt").getInt(obj);
            assertEquals(14000, privateInt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

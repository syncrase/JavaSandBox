package fr.syncrase.classes;

import fr.syncrase.interfaces.markerInterface.MultiMarkedClass;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SerializationTest {

    @Test
    void basicSerialization() {

        Serializer serializer = new Serializer("serialized/", "account.dat");

        // Test the serializability
        MultiMarkedClass multiMarked = new MultiMarkedClass(123456, "string");
        multiMarked.setPublicTransientInt(2);
        assertEquals(2, multiMarked.getPublicTransientInt());

        serializer.serialize(multiMarked);
        multiMarked = serializer.deserialize();

        assertEquals(0, multiMarked.getPublicTransientInt());// The transient wasn't serialized

    }
}

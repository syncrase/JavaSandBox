package fr.syncrase.classes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClassWithStaticTest {

    @Test
    void basicUse() {
        assertEquals("CONST", ClassWithStatic.CONST);

        ClassWithStatic.StaticNestedClass staticNestedClass = new ClassWithStatic.StaticNestedClass();
        assertEquals(2, staticNestedClass.getPrivateLong2OfOuterClass());
        assertEquals(0, staticNestedClass.getPrivateLongOfOuterClass());
    }
}

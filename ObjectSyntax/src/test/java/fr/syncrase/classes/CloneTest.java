package fr.syncrase.classes;

import fr.syncrase.interfaces.markerInterface.MultiMarkedClass;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CloneTest {

    @Test
    void basicClone() {

        MultiMarkedClass multiMarked = new MultiMarkedClass(123456, "string");
        try {
            MultiMarkedClass multiMarked2 = (MultiMarkedClass) multiMarked.clone();
            assertEquals(multiMarked2, multiMarked);
            assertNotSame(multiMarked2, multiMarked);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}

package fr.syncrase.classes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClassWithFinalTest {

    /**
     * Three ways to initialize a final variable :<ul>
     *     <li>At the same time as the declaration</li>
     *     <li>In the constructor</li>
     *     <li>In the instance initializer (<a href="https://docs.oracle.com/javase/specs/jls/se7/html/jls-8.html#jls-8.6">jls-8.6</a>)</li>
     * </ul>
     */
    @Test
    void finalValues() {
        ClassWithFinal classWithFinal = new ClassWithFinal();
        assertEquals(0, classWithFinal.getPrivateFinalLong());
        assertEquals(10, classWithFinal.getPrivateFinalLong2());
        assertEquals(20, classWithFinal.getPrivateFinalLong3());
    }

}

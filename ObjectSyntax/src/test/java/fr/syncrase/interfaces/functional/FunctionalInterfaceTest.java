package fr.syncrase.interfaces.functional;

import fr.syncrase.interfaces.functionalInterface.FuncInterface;
import fr.syncrase.interfaces.functionalInterface.UseFunctionalInterface;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FunctionalInterfaceTest {

    @Test
    void useOfFunctionalInterface() {

        FuncInterface<Integer> fi = Integer::sum;
        FuncInterface<Integer> fi2 = (i, d) -> i * d;
        assertEquals(2, fi.doSomething(1, 1));
        assertEquals(1, fi2.doSomething(1, 1));
    }

    @Test
    void functionalInterfaceAsParameter() {

        FuncInterface<Integer> fi = Integer::sum;
        FuncInterface<Integer> fi2 = (i, d) -> i * d;

        UseFunctionalInterface<Integer> qsd = new UseFunctionalInterface<Integer>();
        List<Integer> list = IntStream.rangeClosed(0, 10).boxed().collect(Collectors.toList());
        qsd//
            .add(list)//
            .display()//
            .apply(fi)//
            .apply(fi2)//
            .display();
    }
}

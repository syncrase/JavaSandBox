package fr.syncrase.interfaces.interfaceDeclaration;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InterfaceTest {

    @Test
    void propertiesInInterface() {
        assertEquals("qsd", IExampleInterface.MY_STRING);

        IExampleInterface ob = new IExampleInterface() {
            @Override
            public int getInt() {
                return 8;
            }

            @Override
            public String getString() {
                return IExampleInterface.super.getString();
            }
        };
        assertEquals(8, ob.getInt());
        assertEquals("default String0", ob.getString());
        assertEquals(0, IExampleInterface.getSomething());
    }

    @Test
    void implementationTypique() {
        assertEquals("qsd", IExampleInterface.MY_STRING);

        IExampleInterface.MyClass ob = new IExampleInterface.MyClass();
        assertEquals(0, ob.getInt());
        assertEquals("default String0", ob.getString());
        assertEquals(0, IExampleInterface.getSomething());
    }
}

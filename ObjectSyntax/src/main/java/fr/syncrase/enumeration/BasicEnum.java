package fr.syncrase.enumeration;

public enum BasicEnum {
    P1("a"), P2("b");

    private final String val;

    /**
     * An enum constructor is always private. Can't be modified to public
     */
    private BasicEnum(String value) {
        this.val = value;
    }

    public String getVal() {
        return val;
    }
}

package fr.syncrase.enumeration;

public enum MethodsEnum {
    SET_OF_METHODS {
        @Override
        public String method() {
            return "item1 method1";
        }

        @Override
        public String method2() {
            return "item1 method2";
        }
    },
    ANOTHER_SET_OF_METHODS {
        @Override
        public String method() {
            return "item2 method1";
        }

        @Override
        public String method2() {
            return "item2 method2";
        }
    };

    public abstract String method(); // could also be in an interface that MyEnum implements

    public abstract String method2(); // could also be in an interface that MyEnum implements
}

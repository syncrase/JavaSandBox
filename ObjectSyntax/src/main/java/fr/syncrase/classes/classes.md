

# Déclarer une variable en Java

## La visibilité
- public
- protected
- private

## L'accessibilité
static : accessible without instancing

## La persistance lors de la serialisation
transient : Préviens la serialisation de la variable 

## La zone mémoire (hors thread)
volatile : La variable est stockée dans la mémoire principale et pas dans
celle du Thread, donc la modification de la variable est propagée aux autres
Thread. Rend impossible tout deadlock

## L'utilisation en tant que constante
final : must be initialize at the latest in the constructor

## Le type (obligatoire)
- byte
- short
- int
- long
- float
- double
- char
- ? extends Object


# Des notions qui s'opposent :
Associer les mots-clés static et transient. 
Transient prévient la variable de la sérialisation de l'instance et static précise que la variable n'appartient pas à l'instance. Donc transient + static ne sert à rien puisque la variable n'est de toute façon pas sérialisée.

Associer les mots-clés final et transient. 
Lors de la désérialisation il ne sera pas possible d'assigner une valeur à la valeur final qui aura été initialisé avec une valeur par défaut
On n'utilise donc pas de notion de final dans une classe serializable
 
Une classe final abstract.
Final signifie que je **ne peux pas étendre** cette classe quand abstract signifie qu'elle **doit être étendue** pour implémenter les méthodes abstraites

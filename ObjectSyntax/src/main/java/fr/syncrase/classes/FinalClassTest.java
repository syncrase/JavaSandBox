package fr.syncrase.classes;

public class FinalClassTest /*
Utilisation de "extends" impossible
extends FinalClass
*/{

    public String myKillerFeature(int[] toti) {
        /*
        Utilisation d'une Classe anonyme impossible
        new FinalClass(){{}}
        */
        return "";
    }

}

final class FinalClass {

    public String myKillerFeature(int[] toti) {
        return "";
    }

}

package fr.syncrase.classes;

public class ClassWithFinal {

    private final long privateFinalLong = 0;
    private final long privateFinalLong2;
    private final long privateFinalLong3;

    {
        System.out.println("instance initialization. I'm before the constructor but after the super()");
        privateFinalLong3 = 20;
    }

    public ClassWithFinal() {
        super();
        System.out.println("Constructor after super");
        privateFinalLong2 = 10 + privateFinalLong;
    }

    public long getPrivateFinalLong() {
        return privateFinalLong;
    }

    public long getPrivateFinalLong2() {
        return privateFinalLong2;
    }

    public long getPrivateFinalLong3() {
        return privateFinalLong3;
    }
}

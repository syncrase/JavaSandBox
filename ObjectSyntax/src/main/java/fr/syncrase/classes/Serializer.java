package fr.syncrase.classes;


import fr.syncrase.interfaces.markerInterface.MultiMarkedClass;

import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * There's no core implementation to serialize, we must do it ourself with
 * ObjectOutputStream and ObjectInputStream
 *
 * @author syncrase
 *
 */
public class Serializer {

    private final Path file;

    public Serializer(String folder, String filename) {
        file = FileSystems.getDefault().getPath(folder, filename);
    }

    public void serialize(MultiMarkedClass acct) {

        try {
            Files.createDirectories(file.getParent());
        } catch (IOException e1) {
            System.out.println(e1.getLocalizedMessage());
        }

        try (ObjectOutputStream objectStream = new ObjectOutputStream(Files.newOutputStream(file))) {
            objectStream.writeObject(acct);

        } catch (NotSerializableException e) {
            System.out.println("Cette classe n'est pas serializable");
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public MultiMarkedClass deserialize() {
        MultiMarkedClass acct = null;
        try (ObjectInputStream objectStream = new ObjectInputStream(Files.newInputStream(file))) {
            acct = (MultiMarkedClass) objectStream.readObject();
            return acct;
        } catch (NotSerializableException e) {
            System.out.println("Cette classe n'est pas serializable");
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getLocalizedMessage());
        }
        return null;
    }

}

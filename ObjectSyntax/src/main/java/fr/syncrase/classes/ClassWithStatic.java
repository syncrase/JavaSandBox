package fr.syncrase.classes;

import java.io.Serializable;

public class ClassWithStatic {

    // Two ways to initialize a static final variable
    private static final long privateStaticFinalLong = 0;
    private static final long privateStaticFinalLong2;

    public static final String CONST = "CONST";
    /*
     * static block (or static clause) used for static initialization.<br>
     * Code executed only the first time that the class is load into memory<br>
     * https://stackoverflow.com/questions/2943556/static-block-in-java
     *
     * Questions : #todo
     * - Quels sont les patterns que cette clause rend possibles?
     * - Un exemple de cas d'utilisation où l'utilisation de la clause est incontournable?
     *
     */
    static {
        System.out.println("static initialization");
        privateStaticFinalLong2 = 2;
    }

    /*
     * A class nested in a serializable class must also be serializable<br>
     *
     */
    public static class StaticNestedClass {

        public long getPrivateLong2OfOuterClass() {
			return ClassWithStatic.privateStaticFinalLong2;
        }

        public long getPrivateLongOfOuterClass() {
            return ClassWithStatic.privateStaticFinalLong;
        }
    }
}

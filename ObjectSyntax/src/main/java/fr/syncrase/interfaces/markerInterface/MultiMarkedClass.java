package fr.syncrase.interfaces.markerInterface;

import java.io.Serializable;
import java.util.EventListener;

public class MultiMarkedClass implements Serializable, Cloneable, MarkerInterface, EventListener {
	private int j;
	private String s;

	private transient int publicTransientInt;

	public MultiMarkedClass(int j, String s) {
		this.setJ(j);
		this.setS(s);
		this.setPublicTransientInt(111);
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj.getClass() != this.getClass()) {
			return false;
		}

		MultiMarkedClass o = (MultiMarkedClass) obj;
		return o.getJ() == this.getJ() && o.getS().equals(this.getS());
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	/**
	 * No way to be serialized
	 */
	public int getPublicTransientInt() {
		return publicTransientInt;
	}

	public void setPublicTransientInt(int publicTransientInt) {
		this.publicTransientInt = publicTransientInt;
	}
}

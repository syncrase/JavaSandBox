package fr.syncrase.interfaces.functionalInterface;

import java.util.ArrayList;
import java.util.List;

public class UseFunctionalInterface<T> {

    List<T> list;

    public UseFunctionalInterface() {
        this.list = new ArrayList<T>();
    }

    public UseFunctionalInterface<T> add(List<T> collect) {
        list.addAll(collect);
        return this;
    }

    public UseFunctionalInterface<T> apply(FuncInterface<T> fi) {

        list.replaceAll(t -> fi.doSomething(t, t));
        return this;
    }

    public UseFunctionalInterface<T> display() {
        System.out.println(list);
        return this;
    }

}


package fr.syncrase.interfaces.interfaceDeclaration;

public interface ClassesIntoInterface {

	class MyNormalClass {
		static void initializeMyClass(Object obj, String str) {
			initializeMyClass2(obj, str);
		}

		private static void initializeMyClass2(Object obj, String str) {
		}
	}

	final class MyFinalStaticClass {
		static void initializeMyClass(Object obj, String str) {
		}

		private void initializeMyClass2(Object obj, String str) {
		}

		public void initializeMyClass3(Object obj, String str) {
			initializeMyClass2(obj, str);
		}
	}
}

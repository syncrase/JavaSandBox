package fr.syncrase.primitives;

public class Bytes {

    public Bytes() {
    }

    static String getBinaryRepresentation(byte b) {
        String stringRepresentation = Integer.toBinaryString(b & 0xFF);
        String heightCharsLength = String.format("%8s", stringRepresentation);
        return heightCharsLength.replace(' ', '0');
    }

}

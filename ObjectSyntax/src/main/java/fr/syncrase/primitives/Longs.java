package fr.syncrase.primitives;

import org.jetbrains.annotations.NotNull;

import java.math.BigInteger;

public class Longs {

	static @NotNull String getBinaryRepresentation(long b) {
		// Pas possible d'uiliser toBinaryString
		// & 0xFFFFFFFFFFFFFFFF pas possible => utilise int
		String stringRepresentation = Long.toBinaryString(b);
//		String stringRepresentation = Integer.toBinaryString(b & 0xFFFFFFFFFFFFFFFF);
		String heightCharsLength = String.format("%64s", stringRepresentation);
		return heightCharsLength.replace(' ', '0');
	}
}

package fr.syncrase.primitives;

/**
 * <br>
 * <h1>Rappel du calcul binaire</h1>
 * <h2>Les valeurs des puissances de 2</h2>
 * <table border='1' style='isplay:table-header-group;'>
 * <thead>
 * <tr>
 * <th>...</th>
 * <th>2³¹</th>
 * <th>2³⁰</th>
 * <th>2²⁹</th>
 * <th>2²⁸</th>
 * <th>2²⁷</th>
 * <th>2²⁶</th>
 * <th>2²⁵</th>
 * <th>2²⁴</th>
 * <th>2²³</th>
 * <th>2²²</th>
 * <th>2²¹</th>
 * <th>2²⁰</th>
 * <th>2¹⁹</th>
 * <th>2¹⁸</th>
 * <th>2¹⁷</th>
 * <th>2¹⁶</th>
 * <th>2¹⁵</th>
 * <th>2¹⁴</th>
 * <th>2¹³</th>
 * <th>2¹²</th>
 * <th>2¹¹</th>
 * <th>2¹⁰</th>
 * <th>2⁹</th>
 * <th>2⁸</th>
 * <th>2⁷</th>
 * <th>2⁶</th>
 * <th>2⁵</th>
 * <th>2⁴</th>
 * <th>2³</th>
 * <th>2²</th>
 * <th>2¹</th>
 * <th>2⁰</th>
 * </tr>
 * </thead> <tbody>
 * <th>...</th>
 * <td>2147483648</td>
 * <td>1073741824</td>
 * <td>536870912</td>
 * <td>268435456</td>
 * <td>134217728</td>
 * <td>67108864</td>
 * <td>33554432</td>
 * <td>16777216</td>
 * <td>8388608</td>
 * <td>4194304</td>
 * <td>2097152</td>
 * <td>1048576</td>
 * <td>524288</td>
 * <td>262144</td>
 * <td>131072</td>
 * <td>65536</td>
 * <td>32768</td>
 * <td>16384</td>
 * <td>8192</td>
 * <td>4096</td>
 * <td>2048</td>
 * <td>1024</td>
 * <td>512</td>
 * <td>256</td>
 * <td>128</td>
 * <td>64</td>
 * <td>32</td>
 * <td>16</td>
 * <td>8</td>
 * <td>4</td>
 * <td>2</td>
 * <td>1</td>
 * </tr>
 * </tbody>
 * </table>
 * 8 bit permettent de coder 2⁹-1 = 511 valeurs. Résulte l'interval [0 ;
 * 510].<br>
 * Pour rappel, 1 octet = 1 byte = 8 bit<br>
 * <h2>Rappel du complément à 2</h2> <br>
 * Prenons comme base un octet (8 bit) signé => <br>
 * <ul>
 * <li>le signe est porté par le bit de poid le plus lourd.</li>
 * <li>les valeurs sont donc codées sur 7 bit. Valeur max = 64 + 32 + ... + 1 =
 * 127</li>
 * <li>Il en résulte l'interval [-128 ; 127] dans lequel est codé 256
 * valeurs</li>
 * <li>Donc 01111111 (127) + 1 = 10000000 (-128)</li>
 * <li>Donc 11111111 (256) = 00000000</li>
 * </ul>
 * <br>
 * <br>
 * <h1>Il existe 8 types primitif en Java</h1>
 *
 * <table border='1'>
 * <thead>
 * <tr>
 * <th>Mot-clé</th>
 * <th>Syntaxe</th>
 * <th>Represent</th>
 * <th>Memory</th>
 * <th>Signed</th>
 * </tr>
 * </thead> <tbody>
 * <tr>
 * <td>boolean</td>
 * <td>true or false</td>
 * <td>Boolean</td>
 * <td>1 bit</td>
 * <td>no</td>
 * </tr>
 * <tr>
 * <td>byte</td>
 * <td>0</td>
 * <td>Byte</td>
 * <td>1 octet (8 bit)</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>short</td>
 * <td>0</td>
 * <td>Short integer</td>
 * <td>2 octets (16 bit)</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>char</td>
 * <td>'\u0000'</td>
 * <td>Character</td>
 * <td>2 octets (16 bit)</td>
 * <td>no</td>
 * </tr>
 * <tr>
 * <td>int</td>
 * <td>0</td>
 * <td>Integer</td>
 * <td>4 octets (32 bit)</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>long</td>
 * <td>0L</td>
 * <td>Long integer</td>
 * <td>8 octets (64 bit)</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>float</td>
 * <td>0.0f</td>
 * <td>Float number</td>
 * <td>4 octets IEEE 754 floating point (32 bit)</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>double</td>
 * <td>0.0d</td>
 * <td>Double precision float number</td>
 * <td>8 octets IEEE 754 floating point (64 bit)</td>
 * <td>yes</td>
 * </tr>
 * </tbody>
 * </table>
 *
 * @author syncrase
 *
 */
public class PrimitiveTypes {

	public static void main(String[] args) {

		// Display the characters table
		for (long i = 33; i < 127; i++) {
			System.out.println(" |" + i + "\t|" + (char) i + "\t|");
		}
		System.out.println(" \t (...) ");
		for (long i = 161; i < 256; i++) {
			System.out.println(" |" + i + "\t|" + (char) i + "\t|");
		}

//		byte	0		8-bit signed two's complement integer
//		short	0		16-bit signed two's complement integer
//		int	0			32-bit signed two's complement integer
//		long	0L		64-bit two's complement integer
//		float	0.0f	single-precision 32-bit IEEE 754 floating point
//		double	0.0d
//		char	'\u0000'
//		System.out.println("byte > 8-bit signed two's complement integer");
		System.out.println("(byte) 127 -> " + (byte) 127);
		System.out.println("(byte) 128 -> " + (byte) 128);// -128

//		System.out.println("short > 16-bit signed two's complement integer");
		System.out.println("(short) 32767 -> " + (short) 32767);
		System.out.println("(short) 32768 -> " + (short) 32768);// -32768

//		System.out.println("int > 32-bit signed two's complement integer");
		System.out.println("(int) 2^31 - 2 -> " + (int) (Math.pow(2, 31) - 2));// 2147483646
		System.out.println("(int) 2^31 - 1 -> " + (int) (Math.pow(2, 31) - 1));// 2147483647
		System.out.println("(int) 2^31 -> " + (int) Math.pow(2, 31));// 2147483647 /!\

//		System.out.println("long > 64-bit two's complement integer");
		// TODO
		System.out.println("(long) 2^63 - 2 -> " + (long) (Math.pow(2, 62) - 2d)); // le math.pow ne fonctionne pas avec
																					// la soustraction
		System.out.println("(long) 2^63 - 1 -> " + (long) (Math.pow(2, 62) - 1d));
		System.out.println("(long) 2^63 -> " + (long) Math.pow(2, 62));//
		
		System.out.println("(long) 9223372036854775806L -> " + (long) 9223372036854775806L);//
		System.out.println("(long) 9223372036854775807L -> " + (long) 9223372036854775807L);//

//		System.out.println("float > single-precision 32-bit IEEE 754 floating point");
//		System.out.println("(float) 2^31 - 1 -> " + (float) (Math.pow(2, 31) - 1));
//		System.out.println("(float) 2^31 -> " + (float) Math.pow(2, 31));// -32768

//		System.out.println("double > ");
//		System.out.println("(double) 2^31 - 1 -> " + (double) (Math.pow(2, 31) - 1));
//		System.out.println("(double) 2^31 -> " + (double) Math.pow(2, 31));// -32768

//		System.out.println("char > ");
//		System.out.println("(char) 2^31 - 1 -> " + (char) (Math.pow(2, 31) - 1));
//		System.out.println("(char) 2^31 -> " + (char) Math.pow(2, 31));// -32768
	}
}

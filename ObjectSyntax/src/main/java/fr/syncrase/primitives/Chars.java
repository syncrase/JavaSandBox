package fr.syncrase.primitives;

import org.jetbrains.annotations.NotNull;

public final class Chars {

	public Chars() {
	}

	static @NotNull String getBinaryRepresentation(char c) {
		String stringRepresentation = Integer.toBinaryString(c & 0xFFFF);
		String heightCharsLength = String.format("%16s", stringRepresentation);
		return heightCharsLength.replace(' ', '0');
	}
}

package fr.syncrase.primitives;

public class Ints {

    static String getBinaryRepresentation(int b) {
        String stringRepresentation = Integer.toBinaryString(b & 0xFFFFFFFF);
        String heightCharsLength = String.format("%32s", stringRepresentation);
        return heightCharsLength.replace(' ', '0');
    }
}

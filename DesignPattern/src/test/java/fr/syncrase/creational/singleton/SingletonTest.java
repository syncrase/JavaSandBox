package fr.syncrase.creational.singleton;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * <a href="https://fr.wikipedia.org/wiki/Singleton_(patron_de_conception)">Wikipedia</a>
 * <p>
 *
 * Le Singleton répond à deux exigences :
 * <ul>
 *     <li>garantir qu'une unique instance d'une classe donné sera créée</li>
 *     <li>offrir un point d'accès universel à cette instance.</li>
 * </ul>
 * <p>
 * Les détails de l'implémentation sont les suivants :
 * <ul>
 *     <li>Le constructeur est inaccessible de l'extérieur de la classe</li>
 *     <li>un accesseur unique existe (static)</li>
 *     <li>Le constructeur est appelé une seule fois</li>
 *     <ul>
 *         <li>à l'initialisation du projet</li>
 *         <li>lazy load, lors du premier appel à l'accesseur</li>
 *     </ul>
 *     <li>La classe ne doit pas être étendue (final)</li>
 *     <li>Doit être commun à tous les threads (soit accesseur synchronized soit instanciation lors de la pré-initialisation)</li>
 *     <li>Si le singleton doit être partagé à tous les ClassLoader de la JVM, c'est un poil plus complexe <a href="http://surguy.net/articles/communication-across-classloaders.xml">communication-across-classloaders</a></li>
 *     <li>Si le singleton doit être partagé entre plusieurs JVM, le protocole standard de communication inter-JVM s'appelle RMI</li>
 * </ul>
 */
class SingletonTest {

    @Test
    void testSingleton() {
        Singleton singleton = Singleton.getInstance();
        assertEquals(2, singleton.doSomething());
    }

    @Test
    void testQueLeConstructeurEstPrive() {
        IllegalAccessException illegalAccessException = assertThrows(IllegalAccessException.class, () -> Class.forName("fr.syncrase.creational.singleton.Singleton").getDeclaredConstructor().newInstance());
        assertEquals(
                "class fr.syncrase.creational.singleton.MySingletonTest cannot access a member of class fr.syncrase.creational.singleton.MySingleton with modifiers \"private\"",
                illegalAccessException.getMessage()
        );
    }

    @Test
    void testQueLaClasseEstFinal() {
        DynamicType.Builder<Singleton> singletonSubClasse = new ByteBuddy().subclass(Singleton.class);
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, singletonSubClasse::make);
        assertEquals(
                "Cannot subclass primitive, array or final types: class fr.syncrase.creational.singleton.MySingleton",
                illegalArgumentException.getMessage()
        );
    }

}
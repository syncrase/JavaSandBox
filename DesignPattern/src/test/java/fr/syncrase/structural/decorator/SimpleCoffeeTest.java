package fr.syncrase.structural.decorator;

import fr.syncrase.structural.decorator.Coffee;
import fr.syncrase.structural.decorator.SimpleCoffee;
import fr.syncrase.structural.decorator.decorators.Milk;
import fr.syncrase.structural.decorator.decorators.Sprinkles;
import fr.syncrase.structural.decorator.decorators.Whip;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * <a href="https://fr.wikipedia.org/wiki/D%C3%A9corateur_(patron_de_conception)">Wiki fr</a><br/>
 * <a href="https://en.wikipedia.org/wiki/Decorator_pattern">Wiki en</a><br/>
 * <a href="https://www.tutorialspoint.com/design_pattern/decorator_pattern.htm">tutorials point</a><br/>
 * <a href="https://sourcemaking.com/design_patterns/decorator">source making</a><br/>
 * <p>
 * <br/>
 * Un décorateur permet d'attacher dynamiquement de nouvelles responsabilités à un objet. Les décorateurs
 * offrent une alternative assez souple à l'héritage pour composer de nouvelles fonctionnalités.
 * Permet de compléter le comportement d'une méthode décorée en appelant la méthode du parent, mais en faisant autre chose
 * <p>
 * La classe abstraite qui permet de décorer (ici {@link fr.syncrase.structural.decorator.decorators.CoffeeDecorator}) est une classe qui :<br/>
 * <ul>
 *     <li>agrège la classe décorée ({@link Coffee})</li>
 *     <li>est la façade de la classe décorée</li>
 * </ul>
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SimpleCoffeeTest {

    // Static car sinon un nouvel objet est créé pour chacun des tests
    private static Coffee c = new SimpleCoffee();

    @Test
    @Order(1)
    void testQueLeCafeDeBaseNeContientQueDuCafe() {
        assertEquals(1.0, c.getCost());
        assertEquals("Coffee", c.getIngredients());
    }

    @Test
    @Order(2)
    void testQueQuandDuLaitEstAjouteLePrixAugmente() {
        c = new Milk(c);
        assertEquals(1.5, c.getCost());
        assertEquals("Coffee, Milk", c.getIngredients());
    }

    @Test
    @Order(3)
    void testQueQuandOnSaupoudreLePrixAugmente() {
        c = new Sprinkles(c);
        assertEquals(1.7, c.getCost());
        assertEquals("Coffee, Milk, Sprinkles", c.getIngredients());
    }

    @Test
    @Order(4)
    void testQueQuandOnFouetteLeCafeLePrixAugmente() {
        c = new Whip(c);
        assertEquals(2.4, c.getCost());
        assertEquals("Coffee, Milk, Sprinkles, Whip", c.getIngredients());
    }

    @Test
    @Order(5)
    void testQueQuandOnSaupoudreEncoreLePrixAugmenteEncore() {
        c = new Sprinkles(c);
        assertEquals(2.6, c.getCost());
        assertEquals("Coffee, Milk, Sprinkles, Whip, Sprinkles", c.getIngredients());
    }
}
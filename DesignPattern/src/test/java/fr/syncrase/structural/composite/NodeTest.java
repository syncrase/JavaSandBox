package fr.syncrase.structural.composite;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * <a href="https://en.wikibooks.org/wiki/Computer_Science_Design_Patterns/Composite">wikibook</a><br/>
 * <a href="https://fr.wikipedia.org/wiki/Objet_composite">wikipedia</a>
 * <p>
 * Permet de concevoir une structure d'arbre
 * The point of composite is to apply the same operation to a bunch of elements that share an interface
 */
class NodeTest {


    @InjectMocks
    Intermediate rootNode = initTreeStructure();

    Terminal terminal;

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testQueLePrintTerminalAEteAppele4Fois() {
        //MockitoAnnotations.initMocks(this);
        rootNode.print();
        verify(terminal, times(4)).print();
    }

    @Test
    void testLeLogDesNoeudsIntermediaires() {
        System.setOut(new PrintStream(outputStreamCaptor));

        rootNode.print();
        assertEquals("""
                CompositeGraphic : root
                CompositeGraphic : graphic1
                CompositeGraphic : graphic2
                CompositeGraphic : graphic3""", outputStreamCaptor.toString()
                .trim());
    }

    private Intermediate initTreeStructure() {
        Intermediate tree = new Intermediate("root");
        Intermediate graphic1 = new Intermediate("graphic1");
        terminal = mock(Terminal.class);
        graphic1.add(terminal);
        graphic1.add(terminal);
        graphic1.add(terminal);
        tree.add(graphic1);

        Intermediate graphic2 = new Intermediate("graphic2");
        graphic2.add(terminal);
        tree.add(graphic2);

        Intermediate graphic3 = new Intermediate("graphic3");
        tree.add(graphic3);
        return tree;
    }
}
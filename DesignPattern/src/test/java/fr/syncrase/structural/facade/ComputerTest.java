package fr.syncrase.structural.facade;

import fr.syncrase.structural.facade.content.CPU;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

class ComputerTest {

    @InjectMocks
    Computer computer = new Computer();

    @Mock
    CPU mockCPU;

    @BeforeEach
    void setUp() {
        //mockCPU = Mockito.mock(CPU.class);// Same as annotation
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testFacade() {
        computer.start();
        // assert that execute method from processor was called
        Mockito.verify(mockCPU).execute();
    }
}
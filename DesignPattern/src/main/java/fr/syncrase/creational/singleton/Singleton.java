package fr.syncrase.creational.singleton;

public final class Singleton {

    private static Singleton instance = null;// Lazy load

    private Singleton() {
        super();
    }

    public static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

    public int doSomething() {
        return 2;
    }

}

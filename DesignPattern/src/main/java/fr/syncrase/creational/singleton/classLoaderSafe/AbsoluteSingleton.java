package fr.syncrase.creational.singleton.classLoaderSafe;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


/**
 * There can be only one - ie. even if the class is loaded in several different classloaders,
 * there will be only one instance of the object.
 */
public class AbsoluteSingleton implements SingletonInterface {

    /**
     * This is effectively an instance of this class (although actually it may be instead a
     * java.lang.reflect.Proxy wrapping an instance from the original classloader).
     */
    public static SingletonInterface instance = null;

    /**
     * Retrieve an instance of AbsoluteSingleton from the original classloader. This is a true
     * Singleton, in that there will only be one instance of this object in the virtual machine,
     * even though there may be several copies of its class file loaded in different classloaders.
     */
    public static synchronized SingletonInterface getInstance() {
        if (instance == null) {
            ClassLoader currentClassLoader = AbsoluteSingleton.class.getClassLoader();
            // The root classloader is sun.misc.Launcher package. If we are not in a sun package,
            // we need to get hold of the instance of yourself from the class in the root classloader.
            if (isRootClassLoader(currentClassLoader)) {
                instance = createInstanceFromRootClassLoader(currentClassLoader);
                // We're in the root classloader, so the instance we have here is the correct one
            } else {
                instance = new AbsoluteSingleton();
            }
        }

        return instance;
    }

    private static SingletonInterface createInstanceFromRootClassLoader(ClassLoader currentClassLoader) {
        // So we find our parent classloader
        // And get the other version of our current class
        try {
            // And call its getInstance method - this gives the correct instance of yourself
            Method getInstanceMethod = AbsoluteSingleton.class.getClassLoader()
                    .getParent()
                    .loadClass(AbsoluteSingleton.class.getName())
                    .getDeclaredMethod("getInstance");
            Object otherAbsoluteSingleton = getInstanceMethod.invoke(null);
            // But, we can't cast it to our own interface directly because classes loaded from
            // different classloaders implement different versions of an interface.
            // So instead, we use java.lang.reflect.Proxy to wrap it in an object that *does*
            // support our interface, and the proxy will use reflection to pass through all calls
            // to the object.
            return (SingletonInterface) Proxy.newProxyInstance(currentClassLoader,
                    new Class[]{SingletonInterface.class},
                    new PassThroughProxyHandler(otherAbsoluteSingleton));
            // And catch the usual tedious set of reflection exceptions
            // We're cheating here and just catching everything - don't do this in real code
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException |
                 InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean isRootClassLoader(ClassLoader currentClassLoader) {
        return !currentClassLoader.toString().startsWith("sun.");
    }

    private AbsoluteSingleton() {
    }

    private String value = "";

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
package fr.syncrase.creational.singleton.classLoaderSafe;

public interface SingletonInterface {
    String getValue();
    void setValue(String value);
}

package fr.syncrase.structural.flyweight;
/*
 * https://fr.wikipedia.org/wiki/Poids-mouche_(patron_de_conception)
 * 
 * Lorsque de nombreux (petits) objets doivent être manipulés, mais qu'il serait
 * trop coûteux en mémoire s'il fallait instancier tous ces objets, il est
 * judicieux d'implémenter le poids-mouche.
 * 
 */
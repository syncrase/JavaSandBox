package fr.syncrase.structural.composite;

public interface Node {

	public void print();
}

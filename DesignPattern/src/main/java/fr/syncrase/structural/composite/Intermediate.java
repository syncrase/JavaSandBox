package fr.syncrase.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class Intermediate implements Node {

	// Collection of child graphics.
	private final List<Node> childrenNodes = new ArrayList<Node>();
	private final String name;

	Intermediate(String s) {
		this.name = s;
	}

	// Prints the graphic.
	public void print() {
		System.out.println("CompositeGraphic : " + name);
		for (Node graphic : childrenNodes) {
			graphic.print();
		}
	}

	// Adds the graphic to the composition.
	public void add(Node graphic) {
		childrenNodes.add(graphic);
	}

	// Removes the graphic from the composition.
	public void remove(Node graphic) {
		childrenNodes.remove(graphic);
	}
}

package fr.syncrase.structural.composite;

public class Terminal implements Node {

	private String name;

	public Terminal() {
	}

	public Terminal(String name) {
		this.name = name;
	}

	@Override
	public void print() {
		System.out.println("Ellipse " + name);
	}

}

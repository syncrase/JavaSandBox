package fr.syncrase.structural.facade;

import fr.syncrase.structural.facade.content.CPU;
import fr.syncrase.structural.facade.content.HardDrive;
import fr.syncrase.structural.facade.content.Memory;

/**
 * Façade
 * Du point de vue UML, c'est une simple relation d'agrégation
 * Du point de vue client, ça permet d'utiliser un ensemble d'objets sans le savoir ni savoir comment ils sont utilisés
 * <p>
 * A pour but de cacher une conception et une interface complexe
 */
public class Computer {

    private final long bootSector = 1;
    private final int sectorSize = 1;
    private final long bootAddress = 1;
    private CPU processor;
    private Memory ram;
    private HardDrive hd;

    public Computer() {
        this.processor = new CPU();
        this.ram = new Memory();
        this.hd = new HardDrive();
    }

    public void start() {
        processor.freeze();
        byte[] data = hd.read(bootSector, sectorSize);

        ram.load(bootAddress, data);
        processor.jump(bootAddress);
        processor.execute();
    }

}

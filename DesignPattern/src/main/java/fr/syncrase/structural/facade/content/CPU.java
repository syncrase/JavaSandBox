package fr.syncrase.structural.facade.content;

public class CPU {
	public void freeze() {
	}

	public void jump(long position) {
	}

	public void execute() {
	}
}

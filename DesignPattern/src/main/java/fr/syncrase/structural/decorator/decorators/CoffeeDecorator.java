package fr.syncrase.structural.decorator.decorators;

import fr.syncrase.structural.decorator.Coffee;

public abstract class CoffeeDecorator extends Coffee {

	protected final Coffee decoratedCoffee;
	protected String ingredientSeparator = ", ";

	protected CoffeeDecorator(Coffee decoratedCoffee) {
		this.decoratedCoffee = decoratedCoffee;
	}

	@Override
	public double getCost() {
		return decoratedCoffee.getCost();
	}

	@Override
	public String getIngredients() {
		return decoratedCoffee.getIngredients();
	}

}

package fr.syncrase.beharioral.iterator;
/*
 * https://en.wikipedia.org/wiki/Iterator_pattern
 * https://fr.wikipedia.org/wiki/It%C3%A9rateur
 * 
 * patron de conception (design pattern) comportemental
 * permet de parcourir tous les éléments contenus dans un autre objet
 */
package fr.syncrase.beharioral.chainofresponsability;
/* 
 * https://fr.wikipedia.org/wiki/Cha%C3%AEne_de_responsabilit%C3%A9
 * 
 * permet à un nombre quelconque de classes d'essayer de répondre à une requête sans connaître les possibilités des autres classes sur cette requête
 * 
 */
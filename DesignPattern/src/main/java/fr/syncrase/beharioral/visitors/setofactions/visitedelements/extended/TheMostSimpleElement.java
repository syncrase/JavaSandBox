package fr.syncrase.beharioral.visitors.setofactions.visitedelements.extended;


import fr.syncrase.beharioral.visitors.setofactions.visitedelements.AbstractElement;

public class TheMostSimpleElement extends AbstractElement {

	public TheMostSimpleElement(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

}

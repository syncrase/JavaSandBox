package fr.syncrase.beharioral.visitors.setofactions.visitors;


import fr.syncrase.beharioral.visitors.setofactions.visitedelements.AbstractElement;
import fr.syncrase.beharioral.visitors.setofactions.visitedelements.extended.Element;
import fr.syncrase.beharioral.visitors.setofactions.visitedelements.extended.ElementContainer;
import fr.syncrase.beharioral.visitors.setofactions.visitedelements.extended.TheMostSimpleElement;

public class SetOfActionPrintImpl implements ISetOfAction {

	@Override
	public void performOn(TheMostSimpleElement el) {
		System.out.print(el.getName());
	}

	@Override
	public void performOn(Element el) {
		System.out.print(el.getName());
	}

	@Override
	public void performOn(ElementContainer el) {
		System.out.print(el.getName());
	}

	@Override
	public void performOn(AbstractElement aEl) {
		System.out.print(aEl.getName());

	}

}

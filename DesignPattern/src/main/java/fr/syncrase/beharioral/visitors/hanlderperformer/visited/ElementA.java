package fr.syncrase.beharioral.visitors.hanlderperformer.visited;

import fr.syncrase.beharioral.visitors.hanlderperformer.visitors.hanldler.IHandler;
import fr.syncrase.beharioral.visitors.hanlderperformer.visitors.performer.IPerformer;

public class ElementA extends AbstractElement {

	public ElementA(String name) {
		super();
		this.name = name;
	}

	@Override
	public void performWith(IPerformer performer) {
		performer.performOn(this);
	}

	@Override
	public void handleWith(IHandler handler) {
		handler.handleOn(this);

	}

}

package fr.syncrase.beharioral.visitors.hanlderperformer.visitors.hanldler;

import fr.syncrase.beharioral.visitors.hanlderperformer.visited.AbstractElement;
import fr.syncrase.beharioral.visitors.hanlderperformer.visited.ElementA;
import fr.syncrase.beharioral.visitors.hanlderperformer.visited.ElementB;
import fr.syncrase.beharioral.visitors.hanlderperformer.visited.ElementContainer;

public interface IHandler {

	void handleOn(AbstractElement aEl);

	void handleOn(ElementB el);

	void handleOn(ElementA el);

	void handleOn(ElementContainer el);
}

package fr.syncrase.beharioral.visitors.hanlderperformer.visited;

import fr.syncrase.beharioral.visitors.hanlderperformer.visitors.hanldler.IHandler;
import fr.syncrase.beharioral.visitors.hanlderperformer.visitors.performer.IPerformer;

/**
 * Implement this interface in order to activate the visitor behaviour<br>
 * Defines actions which are available to be dispatch
 * 
 * @author Pierre TAQUET
 *
 */
public interface DispatchedActions {
	/**
	 * 
	 * This method must be override and contain the performer.performOn(this) call
	 * in order to use the IPerformer(Object o) overloading
	 *
	 */
	void performWith(IPerformer performer);

	void handleWith(IHandler handler);
}

/**
 * The point of visitor is to extend a bunch of elements with a new operation without changing their implementation nor the caller's 
 * implementation.
 * http://c2.com/cgi/wiki?VisitorPattern
 * https://en.wikibooks.org/wiki/Computer_Science_Design_Patterns/Visitor
 */
package fr.syncrase.beharioral.visitors.setofactions;
/*
 * http://c2.com/cgi/wiki?VisitorPattern
 * https://en.wikibooks.org/wiki/Computer_Science_Design_Patterns/Visitor
 * https://fr.wikipedia.org/wiki/Visiteur_(patron_de_conception)
 * comportemental
 * 
 * The point of visitor is to extend a bunch of elements with a new operation without changing their implementation nor the caller's 
 * implementation.
 * manière de séparer un algorithme d'une structure de données
 * 
 * Ce design pattern se base sur la surcharge des méthodes du set d'actions. Fonction de l'élement re�u, le traitement n'est pas le même
 * Possibilité de coder quelque chose propre à l'instance
 */
package fr.syncrase.beharioral.visitors.setofactions.visitors;


import fr.syncrase.beharioral.visitors.setofactions.visitedelements.AbstractElement;
import fr.syncrase.beharioral.visitors.setofactions.visitedelements.extended.Element;
import fr.syncrase.beharioral.visitors.setofactions.visitedelements.extended.ElementContainer;
import fr.syncrase.beharioral.visitors.setofactions.visitedelements.extended.TheMostSimpleElement;

public interface ISetOfAction {

	void performOn(AbstractElement aEl);

	void performOn(TheMostSimpleElement el);

	void performOn(Element el);

	void performOn(ElementContainer el);
}

package fr.syncrase.beharioral.visitors.hanlderperformer.visitors.hanldler;

import fr.syncrase.beharioral.visitors.hanlderperformer.visited.AbstractElement;
import fr.syncrase.beharioral.visitors.hanlderperformer.visited.ElementA;
import fr.syncrase.beharioral.visitors.hanlderperformer.visited.ElementB;
import fr.syncrase.beharioral.visitors.hanlderperformer.visited.ElementContainer;

public class HandlerB implements IHandler {

	/*
	 * Here, some interfaces can be used by each visitor to handle some specific
	 * tasks. Or an abstract class
	 */

	@Override
	public void handleOn(AbstractElement aEl) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleOn(ElementB el) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleOn(ElementA el) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleOn(ElementContainer el) {
		// TODO Auto-generated method stub

	}

}

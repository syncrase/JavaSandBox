package fr.syncrase.beharioral.visitors.hanlderperformer.visitors.performer;


import fr.syncrase.beharioral.visitors.hanlderperformer.visited.AbstractElement;
import fr.syncrase.beharioral.visitors.hanlderperformer.visited.ElementA;
import fr.syncrase.beharioral.visitors.hanlderperformer.visited.ElementB;
import fr.syncrase.beharioral.visitors.hanlderperformer.visited.ElementContainer;


public interface IPerformer {

	void performOn(AbstractElement aEl);

	void performOn(ElementB el);

	void performOn(ElementA el);

	void performOn(ElementContainer el);
}

package fr.syncrase.beharioral.command;
/* 
 *
 * https://fr.wikipedia.org/wiki/Commande_(patron_de_conception)
 * 
 * patron de conception (design pattern) de type comportemental qui encapsule la notion d'invocation
 * permet de séparer complètement le code initiateur de l'action, du code de l'action elle-même
 */

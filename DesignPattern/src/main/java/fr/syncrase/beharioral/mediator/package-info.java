package fr.syncrase.beharioral.mediator;
/*
 * https://en.wikipedia.org/wiki/Mediator_pattern
 * https://fr.wikipedia.org/wiki/M%C3%A9diateur_(patron_de_conception)
 * 
 * Fournit une interface unifiée pour un ensemble d'interfaces d'un
 * sous-système. Il est utilisé pour réduire le couplage entre plusieurs
 * classes.
 * 
 */
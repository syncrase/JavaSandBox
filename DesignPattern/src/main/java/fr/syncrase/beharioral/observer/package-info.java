package fr.syncrase.beharioral.observer;
/*
 * 
 * https://fr.wikipedia.org/wiki/Observateur_(patron_de_conception)
 * 
 * Pour envoyer un signal à des modules qui jouent le rôle d'observateurs. En
 * cas de notification, les observateurs effectuent alors l'action adéquate en
 * fonction des informations qui parviennent depuis les modules qu'ils observent
 * (les observables).
 */
package fr.syncrase;

import java.io.*;

public class Helper {

    public static Reader openReader(String string) throws FileNotFoundException {

        Reader reader = new FileReader(string);

        return reader;
    }

    public static Writer openWriter(String string) throws IOException {

        FileWriter writer = new FileWriter(string);

        return writer;
    }

}
